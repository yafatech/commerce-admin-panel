import * as React from 'react';
import {styled, useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import SearchIcon from "@mui/icons-material/Search";
import SysBadge from "../widgets/SysBadge";
import NotificationsIcon from "@mui/icons-material/Notifications";
import Avatar from "@mui/material/Avatar";
import {useDispatch, useSelector} from "react-redux";
import {alpha, Button, ButtonBase, Chip, Stack, Tooltip} from "@mui/material";
import InputBase from "@mui/material/InputBase";
import {NavMenu} from "../../commons/NavMenu";
import logo from '../../assets/images/logo2.png';
import Paper from "@mui/material/Paper";
import LogoutIcon from '@mui/icons-material/Logout';
import UIStack from "./UIStack";
import {logoutUser} from "../../redux/slicers/UserSlicer";
import {Link, useNavigate} from "react-router-dom";
import SysBreadComb from "../widgets/SysBreadComb";
import {controlCurrentPage} from "../../redux/slicers/AppSlicer";

const drawerWidth = 200;

const openedMixin = (theme) => ({
    width: drawerWidth,
    background: 'rgb(33, 43, 54)',
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    background: 'rgb(33, 43, 54)',
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({theme}) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    // padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({theme, open}) => ({
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: 'rgb(33, 43, 54)',
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, {shouldForwardProp: (prop) => prop !== 'open'})(
    ({theme, open}) => ({
        width: drawerWidth,
        backgroundColor: 'rgb(33, 43, 54)',
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);

export default function AppDrawer(props) {
    const {children} = props;
    let navigate = useNavigate();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const avatar = useSelector(state => state.user.user.avatar);
    const username = useSelector(state => state.user.user.username);
    const Search = styled('div')(({theme}) => ({
        position: 'relative',
        border: '2px solid #2bb3c0',
        borderRadius: theme.shape.borderRadius,
        color: '#2bb3c0',
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        // backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    }));

    const SearchIconWrapper = styled('div')(({theme}) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));
    const StyledInputBase = styled(InputBase)(({theme}) => ({
        color: 'white',
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(6)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '25ch',
                '&:focus': {
                    width: '40ch',
                },
            },
        },
    }));


    const dispatch = useDispatch();

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleLogout = () => {
        dispatch(logoutUser())
    }

    const handleUserProfile = () => {
        navigate("/app/profile");
    }

    const navToPage = (path) => {
        navigate(path);
        // dispatch to breadComb.
        let p = path.split('/')[2];
        if (p !== 'dashboard') {
            dispatch(controlCurrentPage(p));
        } else {
            dispatch(controlCurrentPage(''));
        }

    }

    return (
        <Box sx={{display: 'flex'}}>
            <CssBaseline/>
            <AppBar position="fixed" open={open} color={'inherit'}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            color: '#2bb3c0',
                            marginRight: '36px',
                            ...(open && {display: 'none'}),
                            '&:hover': {
                                color: '#2bb3c0',
                            }
                        }}
                    >
                        <MenuIcon/>
                    </IconButton>
                    {/*<Avatar src={logo} variant={"square"}/>*/}
                    <Box sx={{flexGrow: 1}}/>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon sx={{color: 'white'}}/>
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search Dashboard…"
                            inputProps={{'aria-label': 'search'}}
                        />
                    </Search>
                    <Box sx={{flexGrow: 1}}/>
                    <UIStack
                        direction={'row'}>
                        <Chip
                            color="primary"
                            avatar={
                                <SysBadge
                                    color={'primary'}
                                    count={10}
                                >
                                    <NotificationsIcon sx={{
                                        color: 'white !important'
                                    }}/>
                                </SysBadge>
                            }
                            sx={{
                                color: 'white !important'
                            }}
                            label="Notifications"
                            variant="outlined"
                        />
                        <Tooltip title="Logged In User">
                            <Chip
                                onClick={handleUserProfile}
                                color="primary"
                                avatar={<Avatar src={avatar}/>}
                                label={username}
                                sx={{
                                    color: 'white !important'
                                }}
                                variant="outlined"
                            />
                        </Tooltip>
                        <Chip
                            onClick={handleLogout}
                            color="primary"
                            variant="outlined"
                            icon={<LogoutIcon sx={{color: 'white !important'}}/>}
                            sx={{
                                color: 'white !important'
                            }}
                            label="Leave"/>
                    </UIStack>
                </Toolbar>
            </AppBar>
            <Drawer variant="permanent" open={open}>
                <Stack direction={'row'} alignItems={'start'} alignContent={'start'}>
                    {open ?
                        <Box component="span" sx={{display: {xs: 'none', md: 'block'}, flexGrow: 1}}>
                            <ButtonBase disableRipple component={Link} to={'/dashboard/default'}>
                                <img src={logo} alt="Berry" width="150"/>
                            </ButtonBase>
                        </Box>
                        : ''}
                    <DrawerHeader>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon sx={{color: '#2bb3c0'}}/> :
                                <ChevronLeftIcon sx={{color: '#2bb3c0'}}/>}
                        </IconButton>
                    </DrawerHeader>
                </Stack>

                <Divider/>
                <List>
                    {NavMenu.map((item, index) => (
                        item.disabled ?
                            <Tooltip title={'coming soon'} arrow>
                                <span>
                                      <ListItem components={Button} button key={index}
                                                onClick={() => navToPage(item.path)}
                                                sx={{
                                                    "&$selected": {
                                                        borderColor: "solid 1px #2bb3c0",
                                                        // color: "#2bb3c0",
                                                        "& .MuiListItemIcon-root": {
                                                            color: "#2bb3c0"
                                                        }
                                                    },
                                                    "&$selected:hover": {
                                                        borderColor: "solid 1px #2bb3c0",
                                                        // color: "white",
                                                        "& .MuiListItemIcon-root": {
                                                            borderColor: "solid 1px #2bb3c0",
                                                        }
                                                    },
                                                    "&:hover": {
                                                        borderColor: "solid 1px #2bb3c0",
                                                        // color: "white",
                                                        "& .MuiListItemIcon-root": {
                                                            borderColor: "solid 1px #2bb3c0",
                                                        }
                                                    },
                                                    selected: {}
                                                }}
                                                disabled={item.disabled}
                                      >
                                        <ListItemIcon sx={{color: '#2bb3c0'}}>
                                            {item.icon}
                                        </ListItemIcon>
                                        <ListItemText sx={{color: 'white', fontSize: '2rem !important'}}
                                                      primary={item.title}/>

                                    </ListItem>
                                </span>

                            </Tooltip>
                            :
                            <ListItem button key={index} onClick={() => navToPage(item.path)}
                                      sx={{
                                          "&$selected": {
                                              borderColor: "solid 1px #2bb3c0",
                                              // color: "#2bb3c0",
                                              "& .MuiListItemIcon-root": {
                                                  color: "#2bb3c0"
                                              }
                                          },
                                          "&$selected:hover": {
                                              borderColor: "solid 1px #2bb3c0",
                                              // color: "white",
                                              "& .MuiListItemIcon-root": {
                                                  borderColor: "solid 1px #2bb3c0",
                                              }
                                          },
                                          "&:hover": {
                                              borderColor: "solid 1px #2bb3c0",
                                              // color: "white",
                                              "& .MuiListItemIcon-root": {
                                                  borderColor: "solid 1px #2bb3c0",
                                              }
                                          },
                                          selected: {}
                                      }}
                            >
                                <ListItemIcon sx={{color: '#2bb3c0'}}>
                                    {item.icon}
                                </ListItemIcon>
                                <ListItemText sx={{color: 'white', fontSize: '2rem !important'}} primary={item.title}/>
                            </ListItem>
                    ))}
                </List>
            </Drawer>
            <Box component="section" sx={{p: 1, mb: 2, width: '100%'}}>
                <DrawerHeader/>
                <SysBreadComb/>
                <Paper elevation={22}
                       sx={{mt: 1, p: 1, maxWidth: '100%', width: '100%', backgroundColor: 'rgb(33, 43, 54)'}}>
                    {children}
                </Paper>
            </Box>
        </Box>
    );
}

