import React from "react";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import {AppBar} from "@mui/material";
import SystemProgress from "../widgets/SystemProgress";
import {useSelector} from "react-redux";
function Copyright(props) {
    return (
        <Typography variant="body2" color={'primary'} align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" target='_blank' href="https://yafatek.dev">
                YafaTek Solutions
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default function Footer() {
    const isInvLoading = useSelector(state => state.inventory.isLoading);
    return (
        <AppBar position="fixed"  sx={{top: 'auto', bottom: 0, background: 'rgb(33, 43, 54)'}}>
            <SystemProgress loading={isInvLoading === 'loading'} color={'primary'}/>
            <Copyright/>
        </AppBar>
    );
};