const lightPalette = {
    purpleRedTheme: {
        palette: {
            primary: {
                light: '#EDE7F6',
                main: '#2bb3c0',
                dark: '#2bb3c3',
                contrastText: '#fff',
            },
            secondary: {
                light: '#FCE4EC',
                main: '#EC407A',
                dark: '#C2185B',
                contrastText: '#fff',
            },
        },
    },
    cyanTheme: {
        palette: {
            primary: {
                light: '#E0F7FA',
                main: '#009688',
                dark: '#00695C',
                contrastText: '#fff',
            },
            secondary: {
                light: '#F1F8E9',
                main: '#689F38',
                dark: '#33691E',
                contrastText: '#fff',
            },
        },
    },
    skyBlueTheme: {
        palette: {
            primary: {
                light: '#E3F2FD',
                main: '#2bb3c0',
                dark: '#2bb3c1',
                contrastText: '#fff',
            },
            secondary: {
                light: '#E0F2F1',
                main: '#00BFA5',
                dark: '#00796B',
                contrastText: '#fff',
            },
        },
    },
    blueCyanTheme: {
        palette: {
            primary: {
                light: '#E1F5FE',
                main: '#039BE5',
                dark: '#01579B',
                contrastText: '#fff',
            },
            secondary: {
                light: '#E0F7FA',
                main: '#00BCD4',
                dark: '#00838F',
                contrastText: '#fff',
            },
        },
    },

};

export default lightPalette;
