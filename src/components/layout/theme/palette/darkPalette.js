const darkPalette = {
    cyanTheme: {
        palette: {
            primary: {
                light: '#E0F7FA',
                main: '#26A69A',
                dark: '#00695C',
                contrastText: '#fff',
            },
            secondary: {
                light: '#F1F8E9',
                main: '#689F38',
                dark: '#33691E',
                contrastText: '#fff',
            },
        },
    },
    skyBlueTheme: {
        palette: {
            primary: {
                light: '#E3F2FD',
                main: '#42A5F5',
                dark: '#1565C0',
                contrastText: '#fff',
            },
            secondary: {
                light: '#E0F2F1',
                main: '#00BFA5',
                dark: '#00796B',
                contrastText: '#fff',
            },
        },
    },
    blueCyanTheme: {
        palette: {
            primary: {
                light: '#E0F7FA',
                main: '#00BCD4',
                dark: '#00838F',
                contrastText: '#fff',
            },
            secondary: {
                light: '#E1F5FE',
                main: '#039BE5',
                dark: '#01579B',
                contrastText: '#fff',
            },
            action: {
                disabled: '#039BE5'
            }, input: {
                color: "white"
            }
        },

    }
};

export default darkPalette;
