import * as React from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import BackToTop from "../../widgets/BackToTop";
import {useSelector} from "react-redux";
import Footer from "../Footer";
import AppDrawer from "../AppDrawer";
import themePalette from "./palette/themePaletteMode";
import SystemProgress from "../../widgets/SystemProgress";

function ThemeContainer(props) {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    const {children} = props;
    const langDirection = useSelector(state => state.app.lang)
    const theme = createTheme(themePalette('blueCyanTheme', prefersDarkMode ? 'dark' : 'light'));


    return (
        <ThemeProvider theme={theme}>
            {/*to adapt to color schema, light and dark we're adding color-schema property to css baseline: enableColorScheme*/}
            <CssBaseline enableColorScheme/>
            <div>

                <AppDrawer children={children}/>
                <BackToTop/>
                <Footer/>
            </div>
        </ThemeProvider>
    );
}

export default ThemeContainer;
