import * as React from 'react';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import {styled} from '@mui/material/styles';
import {Divider} from "@mui/material";

/**
 *  component to build ui components in vertical or horizontal directions.
 *  values are: row | row-reverse | column | column-reverse
 * @param props
 * @returns {JSX.Element}
 * @constructor
 * @author Feras E. Alawadi
 */
export default function UIStack(props) {
    const {direction, children, withDividers, spacing} = props;
    return (
            <Stack direction={direction} spacing={spacing ? spacing : 0.5}
                   divider={<Divider
                       sx={{
                           background: '#2bb3c0'
                       }}
                       orientation={withDividers ? direction === 'row' ? 'vertical' : 'horizontal' : ''}
                       flexItem/>}
            >
                {children}
            </Stack>
    );
}
