import React from "react";
import Skeleton from "@mui/material/Skeleton";

export default function SystemLoader(props) {
    const {type, width, height} = props;
    return (
        <Skeleton variant="text" animation="wave" width={40} height={40}/>
    );
};