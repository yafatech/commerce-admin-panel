import * as React from 'react';
import {Snackbar} from "@mui/material";
import {useState} from "react";
import MuiAlert from '@mui/material/Alert';

/**
 * Alerts that can be used among the system.
 * types are :
 * 1- warning
 * 2- error
 * 3- info
 * 4- success
 * @returns {JSX.Element}
 * @constructor
 */
const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
export default function SystemAlerts(props) {
    const {type, open, message, onClose} = props;
    const [vertical] = useState('bottom');
    const [horizontal] = useState('right');

    const handleClose = () => {
        // to close the notification.
        onClose();
    }
    return (
        <Snackbar
            key={vertical + horizontal}
            anchorOrigin={{vertical, horizontal}}
            open={open} autoHideDuration={3000} onClose={handleClose}>
            <Alert
                onClose={handleClose}
                severity={type} sx={{width: '100%'}}>
                {message}
            </Alert>
        </Snackbar>

    );
}
