import React from "react";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Paper from '@mui/material/Paper';
import Draggable from 'react-draggable';

const ConfirmDialog = (props) => {
    const {title, message, open, handleClick, handleClose, color} = props;

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                PaperComponent={PaperComponent}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle style={{cursor: 'move'}} id="draggable-dialog-title" color={color}>
                    {title}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText color={color}>
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button variant={'outlined'} color={'primary'} autoFocus onClick={() => handleClick(false)}>
                        Cancel
                    </Button>
                    <Button variant={'outlined'} onClick={() => handleClick(true)} color={color}>Delete</Button>
                </DialogActions>
            </Dialog>
        </div>
    );

};

function PaperComponent(props) {
    return (
        <Draggable
            handle="#draggable-dialog-title"
            cancel={'[class*="MuiDialogContent-root"]'}
        >
            <Paper {...props} />
        </Draggable>
    );
}

export default ConfirmDialog;