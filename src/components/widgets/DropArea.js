import React, {useCallback, useState} from 'react'
import {useDropzone} from 'react-dropzone'
import {Box, ImageListItemBar, Paper, Typography} from "@mui/material";
import multimedia from '../../assets/images/icons/picture.png';
import UIStack from "../layout/UIStack";
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import IconButton from "@mui/material/IconButton";
import {useDispatch, useSelector} from "react-redux";
import {removeTempImages} from "../../redux/slicers/InventorySlicer";

export default function DropArea(props) {
    let {filesLimit, handleImageChange, isSingleFile} = props;
    const dispatch = useDispatch();
    const [files, setFiles] = useState([]);
    const {userId} = useSelector(state => state.user.user);

    const onDrop = useCallback((selectedFiles) => {
        if (isSingleFile !== undefined && isSingleFile) {
            setFiles(selectedFiles);
            encodeImageFileAsURL(selectedFiles);
        } else {
            if (selectedFiles.length > 1) {
                selectedFiles.forEach(item => {
                    setFiles([...files, item]);
                    encodeImageFileAsURL(item);
                });
            } else {
                setFiles([...files, selectedFiles[0]]);
                encodeImageFileAsURL(selectedFiles);
            }
        }


    }, [files, filesLimit]);

    const handleRemove = useCallback((file, fileIndex) => {
        // This is to prevent memory leaks.
        window.URL.revokeObjectURL(file.preview);

        setFiles(thisFiles => {
            const tempFiles = [...thisFiles];
            // console.log('d:::3',thisFiles[0].path);
            dispatch(removeTempImages(thisFiles[0].path));
            tempFiles.splice(fileIndex, 1);
            return tempFiles;
        });


    }, []);

    function encodeImageFileAsURL(element) {
        const reader = new FileReader();
        // console.log('d:::3', element[0].path);
        reader.onloadend = function () {
            if (userId) {
                handleImageChange({
                    fileName: element[0].path,
                    owner: userId,
                    imageString: reader.result
                });
            }
        }
        reader.readAsDataURL(element[0]);
    }

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})
    const Previews = (props) => {
        let {images} = props;
        return (
            <ImageList sx={{width: 500, height: 450}} cols={3} rowHeight={164}>
                {images.map((file, index) => {
                    const base64Img = URL.createObjectURL(file);
                    return (
                        <ImageListItem key={index.toString()}>
                            <img src={base64Img} alt="preview" loading="lazy" style={{
                                width: '30px !important',
                            }}/>
                            <ImageListItemBar
                                actionIcon={
                                    <IconButton
                                        onClick={handleRemove}
                                        sx={{color: 'rgba(255, 255, 255, 0.54)'}}
                                    >
                                        <DeleteOutlinedIcon color={'error'}/>
                                    </IconButton>
                                }
                            />
                        </ImageListItem>
                    )
                })}
            </ImageList>
        );
    }
    return (
        <div>
            <Paper elevation={24} component={'div'} sx={{
                height: 150,
                background: 'inherit',
                width: '100%',
                m: 1,
                p: 2,
                margin: '0 auto',
                border: 'dashed 3px #00BCD4',
                display: 'block',
                alignItems: 'center',
                textAlign: 'center',
            }}
                   {...getRootProps()}>
                <input {...getInputProps()} />
                {
                    isDragActive ?
                        <Typography variant={'body2'} color={'primary'}>Drop the files here...</Typography>
                        :
                        <UIStack direction={'row'} spacing={3}>
                            <img className={'drop_image'} src={multimedia} alt={'upload image'} height={70} width={70}/>
                            <UIStack direction={'column'} spacing={1}>
                                <Typography variant={'h6'} color={'primary'}>Click to Select...</Typography>
                                <Typography variant={'body2'} color={'primary'}>or Drag and Drop files
                                    here...</Typography>
                            </UIStack>
                        </UIStack>
                }

            </Paper>
            <Box boxShadow={3} sx={{
                height: 200
            }}>
                {files.length > 0 && <Previews images={files}/>}
            </Box>
        </div>
    )
}

