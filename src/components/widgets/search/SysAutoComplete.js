import React from "react";
import {Autocomplete} from "@mui/lab";
import {TextField} from "@mui/material";

export default function SysAutoComplete(props) {
    const {data, fieldName, label, id, fetchResult, error, helperText} = props;

    const handleChange = (event, newValue) => {
        fetchResult(newValue);
    }
    return (
        <Autocomplete
            color={'primary'}
            freeSolo
            disablePortal
            id={id}
            disableClearable
            onInputChange={((event, newValue) => handleChange(event, newValue))}
            options={data.map((option) => option[fieldName])}
            sx={{
                width: 200,
                '.MuiInputLabel-root ': {
                    color: 'white'
                },
                '.MuiFormHelperText-root': {
                    color: 'white'
                }
            }}
            renderInput={(params) => <TextField
                color={'primary'} {...params} label={label}
                error={error && true && error.id === id}
                helperText={error && error.id === id ? error.message : helperText}
            />}
        />
    );
}
;