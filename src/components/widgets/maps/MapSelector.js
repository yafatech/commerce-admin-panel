import React, {useEffect, useState} from "react";
import MapPicker from 'react-google-map-picker'
// Importing geolocated reducer function
import {geolocated} from "react-geolocated";
import {CircularProgress, Paper} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {changeDefaultLocation, manageAppNotifications} from "../../../redux/slicers/AppSlicer";

const DefaultZoom = 15;

function MapSelector(props) {
    const [zoom, setZoom] = useState(DefaultZoom);
    const dispatch = useDispatch();
    const {defaultLocation, isLocationLoading} = useSelector(state => state.app);
    let content;
    const handleChangeLocation = (lat, lng) => {
        dispatch(changeDefaultLocation({lat: lat, lng: lng}))
    }


    useEffect(() => {
        setZoom(DefaultZoom);
        if (props.isGeolocationAvailable) {
            if (!props.isGeolocationEnabled) {
                // console.log('d:::3', props.isGeolocationEnabled);
                if (isLocationLoading === 'idle') {
                    dispatch(manageAppNotifications(true));
                }
            } else {
                if (props.coords) {
                    if (isLocationLoading === 'idle') {
                        dispatch(changeDefaultLocation({
                            lat: props.coords.latitude,
                            lng: props.coords.longitude
                        }))
                    }
                }
            }
        }

        return () => {
            // reset the location on unmount.
            if (isLocationLoading === 'success') {
                // dispatch(resetLocation());
                dispatch(manageAppNotifications(false));
            }
        }
    });

    if (isLocationLoading === 'success') {
        content = <Paper elevation={24}>
            <MapPicker defaultLocation={defaultLocation}
                       zoom={zoom}
                       mapTypeId="roadmap"
                       style={{height: '600px'}}
                       onChangeLocation={handleChangeLocation}
                // onChangeZoom={handleChangeZoom}
                       apiKey='AIzaSyAiWGyVUX0_umi5r4Ycx-_Co50R1vip084'/>
        </Paper>
    } else {
        content = <CircularProgress color="primary"/>
    }
    return (
        <>
            {content}
        </>

    );
}

export default geolocated({
    positionOptions: {
        enableHighAccuracy: false,
    },
    userDecisionTimeout: 5000,
})(MapSelector);