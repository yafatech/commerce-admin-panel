import React from "react";
import {TextField} from "@mui/material";

export default function SysTextField(props) {
    const {isRequired, id, label, helperText, error, handleChange, checkValidity, value} = props;
    // const handleChange=
    return (
        <TextField
            fullWidth
            value={value}
            required={isRequired}
            error={error && true && error.id === id}
            id={id}
            label={label}
            helperText={error && error.id === id ? error.message : helperText}
            variant={'outlined'}
            inputProps={{style: {fontFamily: 'Arial', color: 'white'}}}
            sx={{
                '.MuiInputLabel-root ': {
                    color: 'white'
                },
                '.MuiFormHelperText-root': {
                    color: 'white'
                }
            }}
            onChange={handleChange}
            onBlur={(e) => checkValidity(e)}
        />
    );
};