import * as React from 'react';
import Badge from '@mui/material/Badge';

export default function SysBadge(props) {
    const {children, color, count} = props;
    return (
        <Badge badgeContent={count ? count : 4} color={color ? color : "primary"}>
            {children}
        </Badge>
    );
}
