import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';
import {useTheme} from '@mui/material/styles';

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

export default function SysTabs(props) {
    const {tabArray, children, handleChangeIndex, value, handleChange} = props;
    const theme = useTheme();
    return (
        <Box sx={{width: '100%'}}>
            <Tabs
                value={value}
                variant="fullWidth"
                onChange={handleChange}
                textColor="inherit"
                indicatorColor="secondary"
                aria-label="secondary tabs example"
                sx={{ color: 'white'}}
            >
                {tabArray.map((item, index) => {
                    return <Tab key={index} label={item.label} {...a11yProps(item.index)} />
                })}

            </Tabs>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                {children}
            </SwipeableViews>
        </Box>
    );
};
