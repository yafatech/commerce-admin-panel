import * as React from 'react';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import HomeIcon from '@mui/icons-material/Home';
import BeenhereIcon from '@mui/icons-material/Beenhere';
import Paper from "@mui/material/Paper";
import AppsIcon from '@mui/icons-material/Apps';
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {Button, Chip, Container, Grid, Tooltip} from "@mui/material";
import PaidIcon from '@mui/icons-material/Paid';
import UIStack from "../layout/UIStack";
import Box from "@mui/material/Box";
import SystemLoader from "../layout/loaders/SystemLoader";

export default function SysBreadComb() {
    let navigate = useNavigate();
    let {currentPage} = useSelector(state => state.app);
    const {isSubLoading, info} = useSelector(state => state.subscription);

    function handleClick(event) {
        event.preventDefault();
        const anchor = event.target.closest("a");
        // console.info('You clicked a breadcrumb.', anchor.getAttribute('href'));
        navigate(anchor.getAttribute('href'));
    }

    let currentNav;
    if (currentPage === '') {
        currentNav = ''
    } else {
        if (isSubLoading === 'loading') {
            currentNav = <SystemLoader type={'text'} width={40} height={40}/>
        } else {
            currentNav = <Typography
                sx={{display: 'flex', alignItems: 'center', color: 'white'}}
                color="text.primary"
                variant={'h6'}
            >
                <BeenhereIcon sx={{mr: 0.5}} fontSize="medium" color={'primary'}/>
                {currentPage}
            </Typography>
        }
    }
    let subscriptionContent;
    if (info.subscriptionType === 'FREE') {
        subscriptionContent = <>
            <Tooltip color={'primary'} title="Current Subscription">
                <Chip label={info.subscriptionType} size="medium" color={'warning'}
                      sx={{color: 'yellow'}} variant="outlined"/>
            </Tooltip>
            <Tooltip title="Update Subscription">
                <Button size={'small'} color={'primary'} variant="outlined" sx={{color: 'white'}} endIcon={<PaidIcon/>}>
                    Upgrade
                </Button>
            </Tooltip>
        </>

    } else if (info === '') {
        subscriptionContent = <>
            <Tooltip title="Current Subscription">
                <Chip label="Not Subscribed" size="medium" color={'error'}
                      sx={{color: 'red'}} variant="outlined"/>
            </Tooltip>
            <Tooltip title="Downgrade Subscription">
                <Button size={'small'} color={'error'} variant="outlined" endIcon={<PaidIcon/>}>
                    Subscribe
                </Button>
            </Tooltip>
        </>
    } else {
        subscriptionContent = <>
            <Tooltip color={'primary'} title="Current Subscription">
                <Chip label={info.subscriptionType} size="medium" color={'primary'}
                      sx={{color: 'white'}} variant="outlined"/>
            </Tooltip>
            <Tooltip title="Downgrade Subscription">
                <Button size={'small'} color={'primary'} variant="outlined" endIcon={<PaidIcon/>}>
                    Downgrade
                </Button>
            </Tooltip>
        </>
    }


    return (
        <Paper elevation={22} sx={{mt: 1, p: 1, maxWidth: '100%', width: '100%', backgroundColor: 'rgb(33, 43, 54)',}}>
            <Grid container>
                <Grid item md={3}>
                    <div role="presentation" onClick={handleClick}>
                        <Breadcrumbs aria-label="breadcrumb" sx={{
                            '.MuiBreadcrumbs-separator': {
                                color: '#2bb3c0',
                                fontSize: '1.5rem'
                            }
                        }}>
                            <Link
                                id={'home-bread-comb'}
                                underline="hover"
                                sx={{display: 'flex', alignItems: 'center', color: 'white'}}
                                color="inherit"
                                href="/app/dashboard"
                            >
                                <AppsIcon sx={{mr: 0.5}} color={'primary'} fontSize="medium"/>
                                App
                            </Link>
                            <Link
                                underline="hover"
                                sx={{display: 'flex', alignItems: 'center', color: 'white'}}
                                color="inherit"
                                href="/app/dashboard"
                            >
                                <HomeIcon sx={{mr: 0.5}} color={'primary'} fontSize="medium"/>
                                Dashboard
                            </Link>
                            {currentNav}
                        </Breadcrumbs>
                    </div>
                </Grid>
                <Grid item lg={9} md={9} sm={8}>
                    <Box display="flex" justifyContent="flex-end">
                        <UIStack
                            withDividers={true}
                            spacing={2}
                            direction={'row'}>
                            {subscriptionContent}
                        </UIStack>
                    </Box>
                </Grid>
            </Grid>
        </Paper>
    );
}
