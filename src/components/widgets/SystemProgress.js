import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';

export default function SystemProgress(props) {
    const {loading} = props;
    return (
        loading  ? <LinearProgress color='primary'/> : ''
    );

}
