import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Chip} from "@mui/material";
import {InfoOutlined} from "@mui/icons-material";

export default function StatisticsWidgets(props) {
    const {title, desc, cardImg} = props;
    const handleClick = () => {

    }

    return (
        <Card raised={true} sx={{maxHeight: 170, height: 170, display: 'flex',  background: 'linear-gradient(to right,  rgb(126, 85, 232), rgb(95, 86, 238))'}}>
            <Box sx={{display: 'flex', flexDirection: 'column'}}>
                <CardContent sx={{
                    flex: '1 0 auto',
                }}>
                    <Typography component="h6" variant="h6">
                        {title}
                    </Typography>
                    <Typography variant="body"  component="h3">
                        {desc}
                    </Typography>
                </CardContent>
                <Box sx={{display: 'flex', alignItems: 'center', pl: 1, pb: 1}}>
                    <Chip
                        label="More Info"
                        onClick={handleClick}
                        deleteIcon={<InfoOutlined/>}
                        variant="outlined"
                        sx={{color: 'white'}}
                    />
                </Box>
            </Box>
            <CardMedia
                component="img"
                sx={{width: 160, height: 160}}
                image={cardImg}
                alt="title"
            />
        </Card>
    );
}
