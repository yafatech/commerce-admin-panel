import * as React from 'react';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import {DialogActions, DialogContent, DialogTitle, Fade, Tooltip} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import PropTypes from 'prop-types';
import {styled} from '@mui/material/styles';
import Dialog from "@mui/material/Dialog";
import CloseIcon from '@mui/icons-material/Close';
import {useDispatch, useSelector} from "react-redux";
import Box from "@mui/material/Box";
import WebsiteTemplateUi from "../settings/WebsiteTemplateUi";
import SysTextField from "../widgets/SysTextField";
import LoadingButton from "@mui/lab/LoadingButton";
import SaveIcon from "@mui/icons-material/Save";
import {useState} from "react";
import {cleanDefaultTemplate} from "../../redux/slicers/TemplatesSlicer";
import {updateWebsite} from "../../redux/slicers/InventorySlicer";

export default function EditQrModal(props) {
    const {open, handleClose, item} = props;
    const [newUrl, setNewUrl] = useState('');
    const [newBusinessName, setNewBusinessName] = useState('');
    const dispatch = useDispatch();
    const [error, setError] = useState({
        id: '',
        message: ''
    });
    const isInvLoading = useSelector(state => state.inventory.isLoading);
    const {token} = useSelector(state => state.user.user);
    const {
        defaultTemplate,
    } = useSelector(state => state.templating);

    const handleChange = (event) => {
        let id = event.target.id;
        let value = event.target.value;
        switch (id) {
            case 'newUrl':
                setNewUrl(value);
                break;
            case 'newBusinessName':
                setNewBusinessName(value);
                break;
            default:
                break;
        }

    }
    const checkValidity = (event) => {
        if (event.target.id === 'newUrl') {
            if (newUrl.slice(-1) === '/') {
                // the url contains / at the end, its not allows because the users templates parsed the url whithout the / at the end.
                setError({
                    id: 'newUrl',
                    'message': 'websites Urls are not allowed to have / at the end'
                });
            } else if (newUrl.includes('muzn-software') || newUrl.includes('yafatek')) {
                setError({
                    id: 'newUrl',
                    message: 'you can not use this domain'
                });
            } else if (!newUrl.includes('http') || !newUrl.includes('https')) {
                setError({
                    id: 'newUrl',
                    'message': 'website url must be a valid url starts with http, https and have a valid domain eg: .com, .net'
                });
            } else {
                // reset error object.
                setError({
                    id: '',
                    'message': ''
                });
            }
        }
    }
    const handleUpdate = () => {

        let updatedItem = {
            qrId: item.qrId,
            url: newUrl === '' ? item.url : newUrl,
            businessName: newBusinessName === '' ? item.businessName : newBusinessName,
            templateContainerName: defaultTemplate.templateDockerName,
            templateThumbnail: defaultTemplate.templateImages[0].thumbnail,
            previewUrl: defaultTemplate.templateUrl
        }

        dispatch(updateWebsite({token, website: updatedItem}));

        resetForm();

        // close the modal.
        handleClose();
    }

    const resetForm = () => {
        setNewUrl('');
        setNewBusinessName('');
        dispatch(cleanDefaultTemplate());
    }
    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                sx={{
                    filter: 'blur(3px)',
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                }}
            >
                <div>
                    <BootstrapDialog
                        // onClose={handleClose}
                        aria-labelledby="customized-dialog-title"
                        open={open}
                    >
                        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                            Edit Website Information
                        </BootstrapDialogTitle>
                        <DialogContent dividers>
                            <Box
                                component="form"
                                sx={{
                                    '& .MuiTextField-root': {m: 1, width: '30ch'},
                                }}
                                alignItems={'center'}
                                alignContent={'center'}
                                noValidate
                                autoComplete="off"
                            >

                                <Box component={'div'} sx={{p: 1}}>
                                    <WebsiteTemplateUi/>
                                </Box>
                                <div>
                                    <SysTextField
                                        value={newUrl === '' ? item !== undefined && item.url : newUrl}
                                        error={error}
                                        isRequired={true}
                                        handleChange={handleChange}
                                        checkValidity={checkValidity}
                                        id={'newUrl'}
                                        label={'edit WebSite URL'}
                                        helperText={'eg: https://muzn-software.com'}
                                    />
                                    <SysTextField
                                        value={newBusinessName === '' ? item !== undefined && item.businessName : newBusinessName}
                                        error={error}
                                        isRequired={true}
                                        handleChange={handleChange}
                                        id={'newBusinessName'}
                                        label={'New Business Name'}
                                        helperText={'eg: Muzn Cloths Store'}
                                    />
                                </div>
                                <Box component={'div'} sx={{p: 1}}>
                                    <Tooltip title={'create new website'} arrow TransitionComponent={Fade}
                                             TransitionProps={{timeout: 600}}>
                                        <LoadingButton
                                            color="primary"
                                            loading={isInvLoading === 'loading' && newUrl !== ''}
                                            loadingPosition="start"
                                            startIcon={<SaveIcon/>}
                                            variant="outlined"
                                            onClick={handleUpdate}
                                        >
                                            Update
                                        </LoadingButton>
                                    </Tooltip>
                                </Box>
                            </Box>
                            {/*<ManagedWebSitesForm loading={isInvLoading === 'loading'}  item={item}/>*/}
                        </DialogContent>
                        {/*<DialogActions>*/}
                        {/*    <Button autoFocus onClick={handleClose}>*/}
                        {/*        Save changes*/}
                        {/*    </Button>*/}
                        {/*</DialogActions>*/}
                    </BootstrapDialog>
                </div>
            </Modal>
        </div>
    );
}
const BootstrapDialog = styled(Dialog)(({theme}) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
        backgroundColor: 'rgb(33, 43, 54)'
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
        backgroundColor: 'rgb(33, 43, 54)'
    },
}));
const BootstrapDialogTitle = (props) => {
    const {children, onClose, ...other} = props;

    return (
        <DialogTitle sx={{m: 0, p: 2, backgroundColor: 'rgb(33, 43, 54)'}} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};
