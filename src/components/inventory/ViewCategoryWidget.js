import React, {Fragment, useState} from "react";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import {Button, CardActions, Fade, Tooltip, Typography} from "@mui/material";
import Card from "@mui/material/Card";
import moment from "moment";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import EditCategoryWidget from "./EditCategoryWidget";
import ConfirmDialog from "../alert/ConfirmDialog";
import {useDispatch, useSelector} from "react-redux";
import {clearDeletedCategory, deleteCategory} from "../../redux/slicers/InventorySlicer";


export default function ViewCategoryWidget(props) {
    const {item} = props;
    const [openEdit, setOpenEdit] = useState(false);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const {token} = useSelector(state => state.user.user)
    const dispatch = useDispatch();
    const handleEditOpen = () => {
        setOpenEdit(!openEdit);
    };

    const handleConfirmationClose = () => {
        setIsDeleteOpen(!isDeleteOpen);
    }
    const handleDeleteCategory = (isDelete) => {
        setIsDeleteOpen(!isDeleteOpen);
        if (isDelete) {
            // delete it.
            // alert('delete')
            // remove from redux.
            dispatch(clearDeletedCategory(item.id));
            // dispatch the delete
            dispatch(deleteCategory({token, id: item.id}));
        }
    }
    return (
        <Fragment>
            <Card sx={{maxWidth: 300}}>
                <CardMedia
                    component="img"
                    height="140"
                    image={item.thumbnail}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {item.categoryName}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Belongs to: {item.sourceWebsite}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {item.description}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Created: {moment(item.created, "YYYYMMDD").fromNow()}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Tooltip title={'delete Category'} arrow TransitionComponent={Fade}
                             TransitionProps={{timeout: 600}}>
                        <IconButton aria-label="delete"
                                    onClick={() => setIsDeleteOpen(true)}
                        >
                            <DeleteIcon color={'error'}/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={'edit Category'} arrow TransitionComponent={Fade}
                             TransitionProps={{timeout: 600}}>
                        <IconButton aria-label="edit"
                                    onClick={() => handleEditOpen()}
                        >
                            <EditIcon color={'success'}/>
                        </IconButton>
                    </Tooltip>
                </CardActions>
            </Card>
            <EditCategoryWidget item={item} handleClose={handleEditOpen} open={openEdit}/>
            <ConfirmDialog title={'Deleting ' + item.categoryName}
                           message={"are you sure you want to delete " + item.categoryName + ", all categories and products will be deleted, this can't be reversed"}
                           open={isDeleteOpen}
                           handleClick={handleDeleteCategory}
                           handleClose={handleConfirmationClose}
                           color={'error'}
            />
        </Fragment>
    );
};