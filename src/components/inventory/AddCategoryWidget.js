import React, {useState} from "react";
import Modal from "@mui/material/Modal";
import {styled} from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import {Box, Button, DialogContent, DialogTitle, Fade, TextareaAutosize, TextField, Tooltip} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import PropTypes from "prop-types";
import SysTextField from "../widgets/SysTextField";
import SysAutoComplete from "../widgets/search/SysAutoComplete";
import {useDispatch, useSelector} from "react-redux";
import UIStack from "../layout/UIStack";
import LoadingButton from "@mui/lab/LoadingButton";
import SaveIcon from "@mui/icons-material/Save";
import SystemProgress from "../widgets/SystemProgress";
import {createCategory, handleAddCategoryError, handleTempImages} from "../../redux/slicers/InventorySlicer";
import DropArea from "../widgets/DropArea";
import SystemAlerts from "../alert/SystemAlerts";
import {manageAppNotifications} from "../../redux/slicers/AppSlicer";
import {createAsyncThunk} from "@reduxjs/toolkit";
import {postCDN, postMethod} from "../../utils/NetworkClient";

const BootstrapDialog = styled(Dialog)(({theme}) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
        backgroundColor: 'rgb(33, 43, 54)'
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
        backgroundColor: 'rgb(33, 43, 54)'
    },
}));
export default function AddCategoryWidget(props) {
    // const {managedBusinesses} = useSelector(state => state.inventory);
    const {managedBusinesses, isLoading} = useSelector(state => state.inventory);
    const {token, userId} = useSelector(state => state.user.user);

    const [sourceWebsite, setSourceWebsite] = useState('');
    const [categoryName, setCategoryName] = useState('');
    const [categoryFriendlyName, setCategoryFriendlyName] = useState('');
    const [description, setDescription] = useState('');
    const [thumbnail, setThumbnail] = useState('');
    const [error, setError] = useState({
        id: '',
        message: ''
    });
    const dispatch = useDispatch();

    const handleAutoCompleteChange = (value) => {
        // alert(JSON.stringify(value));
        let url = managedBusinesses.find(item => item.businessName === value);
        setSourceWebsite(url.url);
    }
    const {open, handleClose, title} = props;

    const handleChange = (event) => {
        let id = event.target.id;
        let value = event.target.value;
        switch (id) {
            case 'categoryName':
                setCategoryName(value);
                break;
            case  "categoryFriendlyName":
                setCategoryFriendlyName(value);
                break;
            case "description":
                setDescription(value);
                break;
            default:
                break;
        }
    }

    const checkValidity = () => {
        if (sourceWebsite === '') {
            setError({
                id: 'sourceWebsite',
                message: 'source website must be selected'
            });
        }
    }

    const handleSubmit = () => {
        if (!validateForm()) {
            dispatch(handleAddCategoryError('Please Check your Input.'))
        } else if (thumbnail === '') {
            setError({
                id: '',
                message: ''
            });
            dispatch(handleAddCategoryError('Please Select Category Image'))
        } else {

            postCDN('cdn/media/upload', {
                owner: userId,
                imageString: thumbnail.imageString
            }).then(response => {
                dispatch(createCategory({
                    token, category: {
                        owner: userId,
                        sourceWebsite,
                        categoryName,
                        categoryFriendlyName,
                        thumbnail: response.data.results.fileId,
                        description
                    }
                }));

                // reset the form
                resetForm();

                // close the model.
                handleClose();
            }).catch(err => {
                console.log(err);
            })

        }
    }

    const validateForm = () => {
        if (sourceWebsite === '') {
            setError({
                id: 'sourceWebsite',
                message: 'source website must be selected'
            });
            return false;
        } else if (categoryName === '') {
            setError({
                id: 'categoryName',
                message: 'Category name must be added'
            });
            return false;
        } else if (categoryFriendlyName === '') {
            setError({
                id: 'categoryFriendlyName',
                message: 'Category Friendly Name Must be Added'
            });
            return false;
        } else if (description === '') {
            setError({
                id: 'description',
                message: 'Description Must be Added'
            });
            return false;
        } else {
            return true;
        }
    }
    const resetForm = () => {
        setThumbnail('');
        setCategoryFriendlyName('');
        setCategoryName('');
        setError({
            id: '',
            message: ''
        });
        setDescription('');
        setSourceWebsite('');
    }

    const setImages = (obj) => {
        setThumbnail(obj);
    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-titl1e"
            aria-describedby="modal-modal-descriptio2n"
            sx={{
                filter: 'blur(3px)',
                position: 'absolute',
                width: '100%',
                height: '100%',
            }}
        >
            <BootstrapDialog
                // onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    {title}
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <Box
                        component="form"
                        sx={{
                            '& .MuiTextField-root': {m: 2, width: '30ch'},
                            '& .MuiAutocomplete-root': {width: '30ch'},
                            '& .MuiInputBase-input': {color: 'white'},
                            '& .MuiOutlinedInput-notchedOutline > span': {color: 'white !important'}
                        }}
                        alignItems={'center'}
                        alignContent={'center'}
                        noValidate
                        autoComplete="off"
                    >
                        <div>
                            <UIStack direction={'row'} withDividers={false} spacing={1}>
                                <SysAutoComplete data={managedBusinesses}
                                                 helperText={'please select website'}
                                                 error={error}
                                                 fetchResult={handleAutoCompleteChange}
                                                 fieldName={'businessName'}
                                                 label={'Select Website'}
                                                 id={'sourceWebsite'}/>
                                <SysTextField
                                    value={categoryName}
                                    error={error}
                                    isRequired={true}
                                    handleChange={handleChange}
                                    checkValidity={checkValidity}
                                    id={'categoryName'}
                                    label={'Enter Category Name'}
                                    helperText={'eg: Drinks, Meat'}
                                />
                            </UIStack>
                            <UIStack direction={'row'} withDividers={false} spacing={1}>
                                <SysTextField
                                    value={categoryFriendlyName}
                                    error={error}
                                    isRequired={true}
                                    handleChange={handleChange}
                                    checkValidity={checkValidity}
                                    id={'categoryFriendlyName'}
                                    label={'category Friendly Name'}
                                    helperText={'eg: fires, Tomatoes, T-Shirt'}
                                />
                                <TextField
                                    id="description"
                                    label="Category Description"
                                    value={description}
                                    onChange={handleChange}
                                    onBlur={checkValidity}
                                    multiline
                                    error={error && true && error.id === 'description'}
                                    rows={4}
                                    helperText={error && error.id === 'description' ? error.message : 'fill the category Description'}
                                    variant="outlined"
                                    sx={{
                                        '.MuiInputLabel-root ': {
                                            color: 'white'
                                        },
                                        '.MuiFormHelperText-root': {
                                            color: 'white'
                                        }
                                    }}
                                />
                            </UIStack>
                            <UIStack direction={'column'} withDividers={false} spacing={1}>
                                <Box component={'div'} sx={{p: 1}}>
                                    <DropArea filesLimit={5} handleImageChange={setImages} isSingleFile={true}/>
                                </Box>
                                <Tooltip title={'create new Category'} arrow TransitionComponent={Fade}
                                         TransitionProps={{timeout: 600}}>
                                    <LoadingButton
                                        color="primary"
                                        loading={isLoading === 'loading' && categoryFriendlyName !== ''}
                                        loadingPosition="start"
                                        startIcon={<SaveIcon/>}
                                        variant="outlined"
                                        onClick={handleSubmit}
                                    >
                                        Create
                                    </LoadingButton>
                                </Tooltip>
                            </UIStack>
                        </div>
                        <SystemProgress loading={isLoading === 'loading'} color={'primary'}/>
                    </Box>
                </DialogContent>
            </BootstrapDialog>

        </Modal>

    );
};

const BootstrapDialogTitle = (props) => {
    const {children, onClose, ...other} = props;

    return (
        <DialogTitle sx={{m: 0, p: 2, backgroundColor: 'rgb(33, 43, 54)'}} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};
