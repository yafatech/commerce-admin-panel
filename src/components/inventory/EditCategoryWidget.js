import React, {useState} from "react";
import Modal from "@mui/material/Modal";
import {styled} from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import {Box, DialogContent, DialogTitle, Fade, TextField, Tooltip} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import PropTypes from "prop-types";
import UIStack from "../layout/UIStack";
import SysAutoComplete from "../widgets/search/SysAutoComplete";
import SysTextField from "../widgets/SysTextField";
import DropArea from "../widgets/DropArea";
import LoadingButton from "@mui/lab/LoadingButton";
import SaveIcon from "@mui/icons-material/Save";
import SystemProgress from "../widgets/SystemProgress";
import {useSelector} from "react-redux";

export default function EditCategoryWidget(props) {
    const {item, open, handleClose} = props;
    const {managedBusinesses, isLoading} = useSelector(state => state.inventory);
    const [error, setError] = useState({
        id: '',
        message: ''
    });

    const handleAutoCompleteChange = (value) => {

    }
    const handleSubmit = () => {

    }

    const checkValidity = () => {

    }
    const handleChange = (event) => {

    }

    const setImages = (obj) => {

    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-titl1e"
            aria-describedby="modal-modal-descriptio2n"
            sx={{
                filter: 'blur(3px)',
                position: 'absolute',
                width: '100%',
                height: '100%',
            }}
        >
            <BootstrapDialog
                // onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Edit {item.categoryName}
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <Box
                        component="form"
                        sx={{
                            '& .MuiTextField-root': {m: 2, width: '30ch'},
                            '& .MuiAutocomplete-root': {width: '30ch'},
                            '& .MuiInputBase-input': {color: 'white'},
                            '& .MuiOutlinedInput-notchedOutline > span': {color: 'white !important'}
                        }}
                        alignItems={'center'}
                        alignContent={'center'}
                        noValidate
                        autoComplete="off"
                    >
                        <div>
                            <UIStack direction={'row'} withDividers={false} spacing={1}>
                                <SysAutoComplete data={managedBusinesses}
                                                 helperText={'please select website'}
                                                 error={error}
                                                 fetchResult={handleAutoCompleteChange}
                                                 fieldName={'businessName'}
                                                 label={'Select Website'}
                                                 id={'sourceWebsite'}/>
                                <SysTextField
                                    value={item.categoryName}
                                    error={error}
                                    isRequired={true}
                                    handleChange={handleChange}
                                    checkValidity={checkValidity}
                                    id={'categoryName'}
                                    label={'Enter Category Name'}
                                    helperText={'eg: Drinks, Meat'}
                                />
                            </UIStack>
                            <UIStack direction={'row'} withDividers={false} spacing={1}>
                                <SysTextField
                                    value={item.categoryFriendlyName}
                                    error={error}
                                    isRequired={true}
                                    handleChange={handleChange}
                                    checkValidity={checkValidity}
                                    id={'categoryFriendlyName'}
                                    label={'category Friendly Name'}
                                    helperText={'eg: fires, Tomatoes, T-Shirt'}
                                />
                                <TextField
                                    id="description"
                                    label="Category Description"
                                    value={item.description}
                                    onChange={handleChange}
                                    onBlur={checkValidity}
                                    multiline
                                    error={error && true && error.id === 'description'}
                                    rows={4}
                                    helperText={error && error.id === 'description' ? error.message : 'fill the category Description'}
                                    variant="outlined"
                                    sx={{
                                        '.MuiInputLabel-root ': {
                                            color: 'white'
                                        },
                                        '.MuiFormHelperText-root': {
                                            color: 'white'
                                        }
                                    }}
                                />
                            </UIStack>
                            <UIStack direction={'column'} withDividers={false} spacing={1}>
                                <Box component={'div'} sx={{p: 1}}>
                                    <DropArea filesLimit={5} handleImageChange={setImages} isSingleFile={true}/>
                                </Box>
                                <Tooltip title={'create new Category'} arrow TransitionComponent={Fade}
                                         TransitionProps={{timeout: 600}}>
                                    <LoadingButton
                                        color="primary"
                                        loading={isLoading === 'loading' && item.categoryFriendlyName !== ''}
                                        loadingPosition="start"
                                        startIcon={<SaveIcon/>}
                                        variant="outlined"
                                        onClick={handleSubmit}
                                    >
                                        Create
                                    </LoadingButton>
                                </Tooltip>
                            </UIStack>
                        </div>
                        <SystemProgress loading={isLoading === 'loading'} color={'primary'}/>
                    </Box>
                </DialogContent>
            </BootstrapDialog>
        </Modal>
    );
};

const BootstrapDialog = styled(Dialog)(({theme}) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
        backgroundColor: 'rgb(33, 43, 54)'
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
        backgroundColor: 'rgb(33, 43, 54)'
    },
}));
const BootstrapDialogTitle = (props) => {
    const {children, onClose, ...other} = props;

    return (
        <DialogTitle sx={{m: 0, p: 2, backgroundColor: 'rgb(33, 43, 54)'}} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};
