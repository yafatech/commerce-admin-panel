import React, {useState} from "react";
import {Fade, TextField, Tooltip} from "@mui/material";
import SysTextField from "../widgets/SysTextField";
import {useDispatch, useSelector} from "react-redux";
import Box from "@mui/material/Box";
import SysAutoComplete from "../widgets/search/SysAutoComplete";
import LoadingButton from "@mui/lab/LoadingButton";
import SaveIcon from "@mui/icons-material/Save";
import DropArea from "../widgets/DropArea";
import {handleTempImages} from "../../redux/slicers/InventorySlicer";
import MuiPhoneNumber from 'material-ui-phone-number';
import UIStack from "../layout/UIStack";

export default function BusinessInfoForm(props) {
    const {loading} = props;
    const dispatch = useDispatch();
    const [phoneNumber, setPhoneNumber] = useState('');
    const [facebookUrl, setFacebookUrl] = useState('');
    const [instagramUrl, setInstagramUrl] = useState('');
    // const [googleLng, setGoogleLng] = useState('');
    // const [googleLat, setGoogleLat] = useState('');
    const [websiteUrlId, setWebsiteUrlId] = useState('');
    const [imagesList, setImagesList] = useState([]);
    const {managedBusinesses} = useSelector(state => state.inventory);
    const [error, setError] = useState({
        id: '',
        message: ''
    });

    const {token, userId} = useSelector(state => state.user.user);
    const {defaultLocation} = useSelector(state => state.app);
    const handleChange = (event) => {
        let id = event.target.id;
        let value = event.target.value;
        switch (id) {
            case  "facebookUrl":
                setFacebookUrl(value);
                break;
            case "instagramUrl":
                setInstagramUrl(value);
                break;
            default:
                break;
        }

    }
    const checkValidity = (event) => {
        if (!facebookUrl.includes('fb') || !facebookUrl.includes('facebook')) {
            setError({
                id: 'facebookUrl',
                message: 'that is not a valid facebook url'
            });
        } else if (!instagramUrl.includes('instagram')) {
            setError({
                id: 'instagramUrl',
                message: 'that is not a valid Instagram url'
            });
        }
    }

    const handleSubmit = () => {

    }


    const resetForm = () => {
        setFacebookUrl('');
        setInstagramUrl('');
        setPhoneNumber('');

    }

    const setImages = (obj) => {
        setImagesList([...imagesList, obj]);
        // temp.
        dispatch(handleTempImages(obj));
    }
    const handleOnChange = (phone) => {
        // console.log("d:::4", phone);
        setPhoneNumber(phone);
    }
    return (
        <Box
            component="div"
            sx={{
                '& .MuiTextField-root': {m: 2, width: '35ch'},
                '& .MuiAutocomplete-root': { width: '35ch'},
            }}
            alignItems={'center'}
            alignContent={'center'}
            noValidate
            autoComplete="off"
        >
            <div>
                <UIStack direction={'row'} withDividers={false} spacing={5}>
                    <SysAutoComplete data={managedBusinesses}
                                     fieldName={'businessName'}
                                     label={'Select Website'}
                                     id={'select-website-id'}/>

                    <MuiPhoneNumber defaultCountry={'us'}
                                    excludeCountries={['il', 'sy',]}
                                    disableAreaCodes={true}
                                    id={'phone_input'}
                                    variant={'outlined'}
                                    onChange={handleOnChange}/>
                </UIStack>

                <SysTextField
                    value={facebookUrl}
                    error={error}
                    isRequired={true}
                    handleChange={handleChange}
                    checkValidity={checkValidity}
                    id={'facebookUrl'}
                    label={'facebook page'}
                    helperText={'eg: https://fb.com/muzn-software'}
                />
                <SysTextField
                    value={instagramUrl}
                    error={error}
                    isRequired={true}
                    handleChange={handleChange}
                    checkValidity={checkValidity}
                    id={'instagramUrl'}
                    label={'Instagram Page'}
                    helperText={'eg: https://instagram/muzn-software'}
                />
                <TextField
                    disabled
                    id="outlined-disabled"
                    value={defaultLocation.lat}
                    label="Google Location(lat)"
                    variant={'filled'}
                />
                <TextField
                    disabled
                    value={defaultLocation.lng}
                    id="outlined-disabled1"
                    label="Google Location(lng)"
                    variant={'filled'}
                />

                <Box component={'div'} sx={{p: 1}}>
                    <DropArea filesLimit={5} handleImageChange={setImages}/>
                </Box>

                <Box component={'div'} sx={{p: 1, m: 1}}>
                    <Tooltip title={'create new website'} arrow TransitionComponent={Fade}
                             TransitionProps={{timeout: 600}}>
                        <LoadingButton
                            color="primary"
                            loading={loading && phoneNumber !== ''}
                            loadingPosition="start"
                            startIcon={<SaveIcon/>}
                            variant="outlined"
                            onClick={handleSubmit}
                        >
                            Create
                        </LoadingButton>
                    </Tooltip>
                </Box>
            </div>
        </Box>


    );
};