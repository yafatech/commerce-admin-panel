import * as React from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ListSubheader from '@mui/material/ListSubheader';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
import {Dialog, DialogContent, DialogTitle, Typography} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import {useTheme} from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import {styled} from '@mui/material/styles';
import PropTypes from 'prop-types';
import {ShoppingBag} from "@mui/icons-material";
import {useDispatch} from "react-redux";
import {changeDefaultTemplate} from "../../redux/slicers/TemplatesSlicer";

function srcset(image, width, height, rows = 1, cols = 1) {
    return {
        src: `${image}?w=${width * cols}&h=${height * rows}&fit=crop&auto=format`,
        srcSet: `${image}?w=${width * cols}&h=${
            height * rows
        }&fit=crop&auto=format&dpr=2 2x`,
    };
}

export default function TemplateSelector(props) {
    const {open, handleClose, templates} = props;
    const dispatch = useDispatch();
    const changeTemplate = (id) => {
        let template = templates.find(item => item.id === id);
        dispatch(changeDefaultTemplate(template));
        // close after select.
        handleClose();
    }
    const theme = useTheme();
    // const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    return (
        <div>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <TemplateDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Pick up an Awesome Template
                </TemplateDialogTitle>
                <DialogContent>
                    <ImageList sx={{width: '100%', height: 450}}>
                        <ImageListItem key="Subheader" cols={2}>
                            <ListSubheader sx={{
                                background: 'rgb(33, 43, 54)',
                                color: '#2bb3c0'
                            }} component="div">Please Select</ListSubheader>
                        </ImageListItem>
                        {templates.map((item, index) => {

                            return (
                                <ImageListItem key={index}>
                                    <img
                                        src={item.templateImages[0].thumbnail}
                                        width={300}
                                        height={200}
                                        alt={item.templateName}
                                        loading="lazy"
                                        style={{
                                            display: 'block',
                                            width: '100%',
                                            height: 'auto',
                                            objectFit: 'contain'
                                        }}
                                    />
                                    <ImageListItemBar
                                        sx={{
                                            color: '#2bb3c0',
                                            background:
                                                'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
                                                'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
                                        }}
                                        title={<Typography variant={'h5'}
                                        > {item.priceSymbol === 'FREE' ? 'FREE' : item.priceSymbol + ' ' + item.templatePrice}</Typography>}
                                        position="top"
                                        actionPosition="left"
                                    />
                                    <ImageListItemBar
                                        title={item.templateName}
                                        subtitle={item.description}
                                        sx={{
                                            // overflow: 'auto'
                                        }}
                                        actionIcon={
                                            <IconButton
                                                color={'primary'}
                                                aria-label={`info about ${item.title}`}
                                                onClick={() => changeTemplate(item.id)}
                                            >
                                                <ShoppingBag/>
                                            </IconButton>
                                        }
                                    />
                                </ImageListItem>
                            )
                        })}
                    </ImageList>
                </DialogContent>
            </BootstrapDialog>
        </div>

    );
}
const BootstrapDialog = styled(Dialog)(({theme}) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
        background: 'rgb(33, 43, 54)',
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
        background: 'rgb(33, 43, 54)',
        borderRadius: 10
    },
}));
const TemplateDialogTitle = (props) => {
    const {children, onClose, ...other} = props;

    return (
        <DialogTitle sx={{m: 0, p: 2, color: '#2bb3c0', background: 'rgb(33, 43, 54)'}} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: '#2bb3c0',
                    }}
                >
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </DialogTitle>
    );
};

BootstrapDialog.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};


const itemData = [
    {
        img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
        title: 'Breakfast',
        author: '@bkristastucchio',
        rows: 2,
        cols: 2,
        featured: true,
    },
    {
        img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
        title: 'Burger',
        author: '@rollelflex_graphy726',
    },
    {
        img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
        title: 'Camera',
        author: '@helloimnik',
    },
    {
        img: 'https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c',
        title: 'Coffee',
        author: '@nolanissac',
        cols: 2,
    },
    {
        img: 'https://images.unsplash.com/photo-1533827432537-70133748f5c8',
        title: 'Hats',
        author: '@hjrc33',
        cols: 2,
    },
    {
        img: 'https://images.unsplash.com/photo-1558642452-9d2a7deb7f62',
        title: 'Honey',
        author: '@arwinneil',
        rows: 2,
        cols: 2,
        featured: true,
    },
    {
        img: 'https://images.unsplash.com/photo-1516802273409-68526ee1bdd6',
        title: 'Basketball',
        author: '@tjdragotta',
    },
    {
        img: 'https://images.unsplash.com/photo-1518756131217-31eb79b20e8f',
        title: 'Fern',
        author: '@katie_wasserman',
    },
    {
        img: 'https://images.unsplash.com/photo-1597645587822-e99fa5d45d25',
        title: 'Mushrooms',
        author: '@silverdalex',
        rows: 2,
        cols: 2,
    },
    {
        img: 'https://images.unsplash.com/photo-1567306301408-9b74779a11af',
        title: 'Tomato basil',
        author: '@shelleypauls',
    },
    {
        img: 'https://images.unsplash.com/photo-1471357674240-e1a485acb3e1',
        title: 'Sea star',
        author: '@peterlaster',
    },
    {
        img: 'https://images.unsplash.com/photo-1589118949245-7d38baf380d6',
        title: 'Bike',
        author: '@southside_customs',
        cols: 2,
    },
];
