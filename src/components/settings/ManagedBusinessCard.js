import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import PreviewIcon from '@mui/icons-material/Preview';
import DoneIcon from '@mui/icons-material/Done';
import {CardActionArea, Chip, Fade, Tooltip} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from "@mui/material/IconButton";
import EditIcon from '@mui/icons-material/Edit';
import WarningIcon from '@mui/icons-material/Warning';
import {useDispatch, useSelector} from "react-redux";
import {publishUnPublishWebsite} from "../../redux/slicers/InventorySlicer";
import * as PropTypes from "prop-types";

function ConfirmDialog(props) {
    return null;
}

ConfirmDialog.propTypes = {
    message: PropTypes.string,
    open: PropTypes.func,
    handleClick: PropTypes.any,
    title: PropTypes.string
};
export default function ManagedBusinessCard(props) {
    const {item, handleDeleteWebsite, handleEditQrModalOpen} = props;
    const dispatch = useDispatch();
    const {token} = useSelector(state => state.user.user);

    const handleWebsitePublish = (qrId, status) => {
        dispatch(publishUnPublishWebsite({
            token, businessInfo: {
                qrId,
                status
            }
        }));
    }

    return (
        <Card sx={{width: 350}}>
            <CardMedia
                component="img"
                height="140"
                image={item.templateThumbnail}
                alt={item.qrId}
            />
            <CardContent>
                <Typography gutterBottom variant="h6" component="div">
                    {item.businessName}
                    &nbsp;
                    <Tooltip title={item.published ? "Click to unPublish" : 'Click to Publish'} arrow>
                        <Chip
                            size="small"
                            color={item.published ? 'primary' : 'error'}
                            label="status"
                            onClick={() => handleWebsitePublish(item.qrId, !item.published)}
                            icon={item.published ? <DoneIcon/> : <WarningIcon/>}
                        />
                    </Tooltip>
                </Typography>
                <Typography variant="p" color="text.secondary">
                    Business QR
                </Typography>
                <CardActionArea onClick={() => downloadImage(item.qrImage, item.businessName)}>
                    <Tooltip title="Click to Download" arrow>
                        <CardMedia
                            component="img"
                            image={item.qrImage}
                            alt={item.templateContainerName}
                        />
                    </Tooltip>
                </CardActionArea>
            </CardContent>
            <CardActions>
                <Tooltip title={'delete website'} arrow TransitionComponent={Fade}
                         TransitionProps={{timeout: 600}}>
                    <IconButton aria-label="delete" onClick={() => handleDeleteWebsite(item.qrId)}>
                        <DeleteIcon color={'error'}/>
                    </IconButton>
                </Tooltip>
                <Tooltip title={'edit website info'} arrow TransitionComponent={Fade}
                         TransitionProps={{timeout: 600}}>
                    <IconButton aria-label="edit" onClick={() => handleEditQrModalOpen(item)}>
                        <EditIcon color={'success'}/>
                    </IconButton>
                </Tooltip>
                <Tooltip title={'visit website'} arrow TransitionComponent={Fade}
                         TransitionProps={{timeout: 600}}>
                    <IconButton aria-label="preview" target="_blank" href={item.url}>
                        <PreviewIcon color={'success'}/>
                    </IconButton>
                </Tooltip>
            </CardActions>

        </Card>
    );
}
// download qr image
const downloadImage = (url, fileName) => {

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/jpeg',
        },
    })
        .then((response) => response.blob())
        .then((blob) => {
            // Create blob link to download
            const url = window.URL.createObjectURL(
                new Blob([blob]),
            );
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute(
                'download',
                fileName + `.png`,
            );

            // Append to html link element page
            document.body.appendChild(link);

            // Start download
            link.click();

            // Clean up and remove the link
            link.parentNode.removeChild(link);
        });
}