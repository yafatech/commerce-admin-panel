import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Typography from '@mui/material/Typography';
import logo from '../../assets/images/logo.png';
import {Button} from "@mui/material";
import {useSelector} from "react-redux";
import TemplateSelector from "./TemplateSelector";
import {useState} from "react";


export default function WebsiteTemplateUi() {
    const [openTemplateSelector, setOpenTemplateSelector] = useState(false);
    const {templates, defaultTemplate} = useSelector(state => state.templating);

    const handleTemplateSelector = () => {
        setOpenTemplateSelector(!openTemplateSelector);
    }

    return (
        <>
            <Card sx={{maxWidth: 450}}>
                <CardMedia
                    component="img"
                    height="220"
                    image={defaultTemplate !== '' ? defaultTemplate.templateImages[0].thumbnail : logo}
                    alt="Website Template"
                    sx={{
                        maxWidth: 300
                    }}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {defaultTemplate !== '' ? defaultTemplate.templateName : 'Template Title'}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {defaultTemplate !== '' ? defaultTemplate.description : 'Template Description...'}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" variant={'outlined'} onClick={handleTemplateSelector}>Change</Button>
                </CardActions>
            </Card>
            <TemplateSelector open={openTemplateSelector} handleClose={handleTemplateSelector} templates={templates}/>
        </>

    );
}
