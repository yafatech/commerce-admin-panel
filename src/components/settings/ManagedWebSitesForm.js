import React, {useState} from "react";
import Box from "@mui/material/Box";
import SaveIcon from '@mui/icons-material/Save';
import SysTextField from "../widgets/SysTextField";
import WebsiteTemplateUi from "./WebsiteTemplateUi";
import {useDispatch, useSelector} from "react-redux";
import LoadingButton from '@mui/lab/LoadingButton';
import {cleanDefaultTemplate, manageNotifications} from "../../redux/slicers/TemplatesSlicer";
import {createQr} from "../../redux/slicers/InventorySlicer";
import {Fade, Tooltip} from "@mui/material";

export default function ManagedWebSitesForm(props) {
    const {loading} = props;
    const [url, setUrl] = useState('');
    const [businessName, setBusinessName] = useState('');
    const dispatch = useDispatch();
    const [error, setError] = useState({
        id: '',
        message: ''
    });
    const {
        defaultTemplate,
    } = useSelector(state => state.templating);

    const {token, userId} = useSelector(state => state.user.user);

    const handleChange = (event) => {
        let id = event.target.id;
        let value = event.target.value;
        // alert(id)
        switch (id) {
            case 'url':
                setUrl(value);
                break;
            case 'businessName':
                setBusinessName(value);
                break;
            default:
                break;
        }
    }

    const checkValidity = (event) => {
        if (event.target.id === 'url') {
            if (url.slice(-1) === '/') {
                // the url contains / at the end, its not allows because the users templates parsed the url whithout the / at the end.
                setError({
                    id: 'url',
                    'message': 'websites Urls are not allowed to have / at the end'
                });
            } else if (url.includes('muzn-software') || url.includes('yafatek')) {
                setError({
                    id: 'url',
                    message: 'you can not use this domain'
                });
            } else if (!url.includes('http') || !url.includes('https')) {
                setError({
                    id: 'url',
                    'message': 'website url must be a valid url starts with http, https and have a valid domain eg: .com, .net'
                });
            } else {
                // reset error object.
                setError({
                    id: '',
                    'message': ''
                });
            }
        }
    }
    const handleSubmit = () => {
        if (!validateForm()) {
            dispatch(manageNotifications({dest: 'apiWarning', message: 'some values are missing', status: true}));
        } else if (defaultTemplate === '') {
            dispatch(manageNotifications({
                dest: 'apiError',
                message: 'Website Template must be selected',
                status: true
            }));
        } else {
            // create  it
            dispatch(createQr({
                token, businessWebsite: {
                    businessName,
                    url,
                    owner: userId,
                    templateContainerName: defaultTemplate.templateDockerName,
                    templateThumbnail: defaultTemplate.templateImages[0].thumbnail,
                    previewUrl: defaultTemplate.templateUrl
                }
            }));
            resetForm();
        }
    }

    const validateForm = () => {
        if (url === '') {
            setError({
                id: 'url',
                message: 'Business URL is Required'
            });
            return false;
        } else if (url.includes('muzn-software') || url.includes('yafatek')) {
            setError({
                id: 'url',
                message: 'you can not use this domain'
            });
            return false;
        } else if (businessName === '') {
            setError({
                id: 'businessName',
                message: 'Business Name is Required'
            });
            return false;
        } else {
            return true;
        }

    }

    const resetForm = () => {
        setUrl('');
        setBusinessName('');
        dispatch(cleanDefaultTemplate());
    }


    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': {m: 1, width: '30ch'},
            }}
            alignItems={'center'}
            alignContent={'center'}
            noValidate
            autoComplete="off"
        >

            <Box component={'div'} sx={{p: 1}}>
                <WebsiteTemplateUi/>
            </Box>
            <div>
                <SysTextField
                    value={url}
                    error={error}
                    isRequired={true}
                    handleChange={handleChange}
                    checkValidity={checkValidity}
                    id={'url'}
                    label={'WebSite URL'}
                    helperText={'eg: https://muzn-software.com'}
                />
                <SysTextField
                    value={businessName}
                    error={error}
                    isRequired={true}
                    handleChange={handleChange}
                    id={'businessName'}
                    label={'Business Name'}
                    helperText={'eg: Muzn Cloths Store'}
                />
            </div>
            <Box component={'div'} sx={{p: 1}}>
                <Tooltip title={'create new website'} arrow TransitionComponent={Fade}
                         TransitionProps={{timeout: 600}}>
                    <LoadingButton
                        color="primary"
                        loading={loading && url !== ''}
                        loadingPosition="start"
                        startIcon={<SaveIcon/>}
                        variant="outlined"
                        onClick={handleSubmit}
                    >
                        Create
                    </LoadingButton>
                </Tooltip>
            </Box>
        </Box>
    );
};