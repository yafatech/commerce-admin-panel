import * as React from 'react';
import Skeleton from '@mui/material/Skeleton';

/**
 * a Loader Variant to show loaders while the data is loading.
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function LoaderVariants(props) {
    const {type, width, height} = props;
    switch (type) {
        case 'text':
            return <Skeleton variant="text" width={width} height={height}/>;
        case 'circular':
            return <Skeleton variant="circular" width={width} height={height}/>;
        default:
            return <Skeleton variant="rectangular" width={width} height={height}/>;
    }
}
