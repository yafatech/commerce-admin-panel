import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {deleteData, getDate, postMethod, replaceData, updateData} from "../../utils/NetworkClient";

export const loadQrData = createAsyncThunk(
    'inventory/qr',
    async (user, thunkApi) => {
        let {token, userId} = user;
        const response = await getDate('Bearer ' + token, "qr?owner=" + userId);
        return response.data;
    }
);

export const createQr = createAsyncThunk(
    'inventory/qr/create',
    async (qr, thunkApi) => {
        const {businessWebsite, token} = qr;
        const response = await postMethod('Bearer ' + token, 'qr', businessWebsite);
        return response.data;
    }
);

export const publishUnPublishWebsite = createAsyncThunk(
    'inventory/websites/change',
    async (business, thunkApi) => {
        const {token, businessInfo} = business;
        const response = await updateData('Bearer ' + token, 'qr', businessInfo);
        return response.data;
    }
);

export const deleteWebsite = createAsyncThunk(
    'inventory/websites/delete',
    async (obj, thunkApi) => {
        let {token, id} = obj;
        const response = await deleteData('Bearer ' + token, "qr?target=" + id);
        return response.data;
    }
);

export const updateWebsite = createAsyncThunk(
    'inventory/websites/update',
    async (obj, thunkApi) => {
        let {token, website} = obj;
        const response = await replaceData('Bearer ' + token, 'qr/dest', website);
        return response.data;
    }
)

export const createCategory = createAsyncThunk(
    'inventory/categories/create',
    async (body, thunkApi) => {
        const {token, category} = body;
        const response = await postMethod('Bearer ' + token, 'inventory/categories', category);
        return response.data;
    }
);

export const loadOwnerCategories = createAsyncThunk(
    'inventory/categories/load',
    async (user, thunkApi) => {
        const {token, userId} = user;
        const response = await getDate('Bearer ' + token, 'inventory/categories?owner=' + userId);
        return response.data;
    }
);
export const deleteCategory = createAsyncThunk(
    'inventory/categories/delete',
    async (obj, thunkApi) => {
        const {token, id} = obj;
        const response = await deleteData('Bearer ' + token, 'inventory/categories/secure?target=' + id);
        return response.data;
    }
)
const InventorySlice = createSlice({
    name: 'categoriesSlice',
    initialState: {
        // idle, loading, success,error,  failed.
        isLoading: 'idle',
        openNotification: false,
        isDeleted: false,
        apiSuccess: '',
        apiError: '',
        apiWarning: '',
        categories: [],
        products: [],
        managedBusinesses: [],
        tempImages: []
    },
    reducers: {
        removeWebsite: (state, action) => {
            state.managedBusinesses = state.managedBusinesses.filter(item => item.qrId !== action.payload);
        },
        handleTempImages: (state, action) => {
            state.tempImages.push(action.payload);
        },
        removeTempImages: (state, action) => {
            state.tempImages = state.tempImages.filter(item => item.fileName !== action.payload);
        },
        handleAddCategoryError: (state, action) => {
            state.apiError = action.payload;
            state.openNotification = true;
        },
        handleOpenNotificationChange: (state, action) => {
            state.openNotification = action.payload;
        },
        clearDeletedCategory: (state, action) => {
            state.categories = state.categories.filter(item => item.id !== action.payload);
        },
        manageInvNotifications: (state, action) => {
            let {dest, message, status} = action.payload;
            switch (dest) {
                case 'apiSuccess':
                    state.apiSuccess = message;
                    state.openNotification = status;
                    break;
                case 'apiError':
                    state.apiError = message;
                    state.openNotification = status;
                    break;
                case 'apiWarning':
                    state.apiWarning = message;
                    state.openNotification = status;
                    break;
                default:
                    state.openNotification = status;
            }
        }
    },
    extraReducers: builder => {
        builder
            .addCase(loadQrData.pending, state => {
                state.isLoading = 'loading'
            })
            .addCase(loadQrData.fulfilled, (state, action) => {
                let {results, message, status, errors} = action.payload;
                if (status) {
                    state.apiWarning = '';
                    state.apiError = '';
                    state.isLoading = 'success';
                    state.managedBusinesses = results;
                    state.apiSuccess = message;
                    state.openNotification = true;
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.isLoading = 'error'
                    state.apiWarning = errors.errorMessage;
                    state.openNotification = true;
                }
            })
            .addCase(loadQrData.rejected, state => {
                state.isLoading = 'failed'
            })
            .addCase(createQr.pending, (state, action) => {
                state.isLoading = 'loading';
            })
            .addCase(createQr.fulfilled, (state, action) => {
                const {results, status, message, errors} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.managedBusinesses.push(results);
                    state.apiSuccess = message;
                    state.openNotification = true;
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.isLoading = 'error';
                    state.apiWarning = errors.errorMessage;
                    state.openNotification = true;
                }
            })
            .addCase(createQr.rejected, state => {
                state.openNotification = true;
                state.apiError = 'failed to create Business Website.'
            })
            .addCase(publishUnPublishWebsite.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(publishUnPublishWebsite.fulfilled, (state, action) => {
                let {status, results, errors, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                    const foundIndex = state.managedBusinesses.findIndex(x => x.qrId === results.qrId);
                    state.managedBusinesses[foundIndex] = results;
                } else {
                    state.isLoading = 'error';
                    state.apiWarning = errors.errorMessage;
                    state.openNotification = true;
                }
            })
            .addCase(publishUnPublishWebsite.rejected, state => {
                state.openNotification = true;
                state.isLoading = 'failed';
                state.apiError = 'Can not update Website status.'
            })
            .addCase(deleteWebsite.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(deleteWebsite.fulfilled, (state, action) => {
                let {status, errors, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                    state.isDeleted = true;
                } else {
                    state.isLoading = 'error';
                    state.apiWarning = errors.errorMessage;
                    state.openNotification = true;
                    state.isDeleted = false;
                }
            })
            .addCase(deleteWebsite.rejected, state => {
                state.isDeleted = false;
                state.openNotification = true;
                state.isLoading = 'failed';
                state.apiError = 'can not delete website, please try again later.'
            })
            .addCase(updateWebsite.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(updateWebsite.fulfilled, (state, action) => {
                let {status, errors, results, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                    const foundIndex = state.managedBusinesses.findIndex(x => x.qrId === results.qrId);
                    state.managedBusinesses[foundIndex] = results;
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.apiWarning = errors.errorMessage;
                    state.isLoading = 'error';
                    state.openNotification = true;
                }
            })
            .addCase(updateWebsite.rejected, state => {
                state.openNotification = true;
                state.isLoading = 'failed';
                state.apiError = 'can not update website, please try again later.'
                state.apiSuccess = '';
                state.apiWarning = '';
            })
            .addCase(createCategory.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(createCategory.fulfilled, (state, action) => {
                let {status, errors, results, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                    state.categories.push(results);
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.apiWarning = errors.errorMessage;
                    state.isLoading = 'error';
                    state.openNotification = true;
                }
            })
            .addCase(createCategory.rejected, state => {

            })

            .addCase(loadOwnerCategories.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(loadOwnerCategories.fulfilled, (state, action) => {
                let {status, errors, results, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                    state.categories = results;
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.apiWarning = errors.errorMessage;
                    state.isLoading = 'error';
                    state.openNotification = true;
                }
            })
            .addCase(deleteCategory.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(deleteCategory.fulfilled, (state, action) => {
                let {status, errors, message} = action.payload;
                if (status) {
                    state.isLoading = 'success';
                    state.apiSuccess = message;
                    state.openNotification = true;
                } else {
                    state.apiSuccess = '';
                    state.apiError = '';
                    state.apiWarning = errors.errorMessage;
                    state.isLoading = 'error';
                    state.openNotification = true;
                }
            })
            .addCase(deleteCategory.rejected, state => {
                state.apiSuccess = '';
                state.apiError = 'can not delete category, try again later!';
                state.apiWarning = '';
                state.isLoading = 'failed';
                state.openNotification = true;
            })
    }
});
export const {
    manageInvNotifications,
    removeWebsite,
    handleTempImages,
    removeTempImages,
    handleAddCategoryError,
    handleOpenNotificationChange,
    clearDeletedCategory
} = InventorySlice.actions;
export default InventorySlice.reducer;