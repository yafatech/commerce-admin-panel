import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {client, postMethod} from "../../utils/NetworkClient";
// get the user out of the storage.
const user = localStorage.getItem('user');
export const loginUserThunk = createAsyncThunk(
    'user',
    async (googleToken, thunkApi) => {
        const response = await postMethod('', 'auth/login', googleToken);
        localStorage.setItem('user', JSON.stringify(response.data.results));
        return response.data
    }
);

const UserSlicer = createSlice({
    name: 'userReducer',
    initialState: {
        isLoading: false,
        openNotification: false,
        isAuth: !!user,
        apiSuccess: '',
        apiError: '',
        user: user ? JSON.parse(user) : {}
    },
    reducers: {
        changeNotificationStatus: (state, action) => {
            state.openNotification = action.payload
        },
        logoutUser: state => {
            state.user = '';
            state.apiSuccess = ''
            state.apiError = ''
            state.isAuth = false;
            state.isLoading = false;
            state.openNotification = false;
            localStorage.removeItem('user')
        }
    },
    extraReducers: builder => {
        builder.addCase(loginUserThunk.pending, (state) => {
            state.isLoading = true;
        });
        builder.addCase(loginUserThunk.fulfilled, (state, action) => {
            if (!action.payload.status) {
                state.apiError = action.payload.errors.errorMessage;
                state.isLoading = false;
                state.openNotification = true;
            } else {
                state.user = action.payload.results;
                state.apiSuccess = action.payload.message;
                state.isLoading = false;
                state.isAuth = true;
                state.openNotification = true;
            }
        });
        builder.addCase(loginUserThunk.rejected, (state, action) => {
            state.apiError = 'failed to connect to APIs';
            state.isLoading = false;
            state.openNotification = true;
        });

    }
});
export const {changeNotificationStatus, logoutUser} = UserSlicer.actions;
export default UserSlicer.reducer;