import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {getDate} from "../../utils/NetworkClient";

export const checkUserSubscription = createAsyncThunk(
    'subscription/check',
    async (user, thunkApi) => {
        const {token, userId} = user;
        const response = await getDate('Bearer '+token, "subs?owner=" + userId);
        return response.data;
    }
)
const SubscriptionSlicer = createSlice({
    name: 'subscription',
    initialState: {
        // idle, loading, success,error,  failed.
        isLoading: 'idle',
        info: '',
        apiSuccess: '',
        apiError: '',
        apiWarning: ''
    },
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(checkUserSubscription.pending, state => {
                state.isLoading = 'loading'
            })
            .addCase(checkUserSubscription.fulfilled, (state, action) => {
                let {payload} = action;
                if (!payload.status) {
                    state.isLoading = 'failed';
                    state.apiWarning = payload.errors.errorMessage;
                } else {
                    state.isLoading = 'failed';
                    state.apiSuccess = payload.message;
                    state.info = payload.results;
                }
            })
            .addCase(checkUserSubscription.rejected, (state, action) => {
                state.isLoading = 'failed';
                state.apiError = action.payload.error.message;
            })
    }

});

export default SubscriptionSlicer.reducer;