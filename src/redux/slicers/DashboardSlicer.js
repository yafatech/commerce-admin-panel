import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {getDate} from "../../utils/NetworkClient";

export const loadDashboardStatistics = createAsyncThunk(
    'dashboard/loadStatistics',
    async (user, thunkApi) => {
        ///todo fetch for specific user.
        let {token, userId} = user;
        const response = await getDate('Bearer ' + token, 'dashboard/loadStatistics?owner=' + userId);
        return response.data;
    }
);
const DashboardSlicer = createSlice({
    name: 'dashboard',
    initialState: {
        // idle, loading, success,error,  failed.
        isLoading: 'idle',
        statistics: '',
        apiError: '',
        apiSuccess: '',
    },
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(loadDashboardStatistics.pending, state => {
                state.isLoading = 'loading';
            })
            .addCase(loadDashboardStatistics.fulfilled, (state, action) => {
                const {payload} = action;
                if (!payload.status) {
                    state.apiError = payload.errors.errorMessage;
                    state.isLoading = 'error';
                } else {
                    state.statistics = payload.results;
                    state.apiSuccess = payload.message;
                    state.isLoading = 'success';
                }
            })
            .addCase(loadDashboardStatistics.rejected, (state, action) => {
                state.isLoading = 'failed';
                state.apiError = action.payload.message;
            })
    }
});

export default DashboardSlicer.reducer;