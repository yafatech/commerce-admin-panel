import {createSlice} from "@reduxjs/toolkit";

const AppSlicer = createSlice({
    name: 'appSlicer',
    initialState: {
        lang: 'en-gb',
        directions: 'ltr',
        currentPage: '',
        isLocationLoading: 'idle',
        defaultLocation: '',
        openNotification: false
    },
    reducers: {
        changeLanguage: (state, action) => {
            state.lang = action.payload;
        },
        changeDefaultLocation: (state, action) => {
            state.defaultLocation = action.payload;
            state.isLocationLoading = 'success';
        },
        resetLocation: state => {
            state.defaultLocation = '';
            state.isLocationLoading = 'idle';
        },
        controlCurrentPage: (state, action) => {
            state.currentPage = action.payload;
        },
        manageAppNotifications: (state,action) => {
            state.openNotification = action.payload;
        }
    }
});
export const {changeLanguage, manageAppNotifications, changeDefaultLocation, resetLocation, controlCurrentPage} = AppSlicer.actions;
export default AppSlicer.reducer;