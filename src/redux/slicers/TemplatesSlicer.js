import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {getDate} from "../../utils/NetworkClient";

export const templatesLoader = createAsyncThunk(
    'templating/load',
    async (_, thunkApi) => {
        const response = await getDate('', 'templating');
        return response.data;
    }
);

const TemplatesSlicer = createSlice({
    name: 'templating',
    initialState: {
        isLoading: 'idle',
        openNotification: false,
        templates: [],
        defaultTemplate: '',
        apiSuccess: '',
        apiError: '',
        apiWarning: '',
    },
    reducers: {
        cleanDefaultTemplate: state => {
            state.defaultTemplate = '';
        },
        changeDefaultTemplate: (state, action) => {
            // let {payload} = action;
            state.defaultTemplate = action.payload;
        },
        manageNotifications: (state, action) => {
            let {dest, message, status} = action.payload;
            switch (dest) {
                case 'apiSuccess':
                    state.apiSuccess = message;
                    state.openNotification = status;
                    break;
                case 'apiError':
                    state.apiError = message;
                    state.openNotification = status;
                    break;
                case 'apiWarning':
                    state.apiWarning = message;
                    state.openNotification = status;
                    break;
                default:
                    state.openNotification = status;
            }
        }
    },
    extraReducers: builder => {
        builder.addCase(templatesLoader.pending, state => {
            state.isLoading = 'loading'
        }).addCase(templatesLoader.fulfilled, (state, action) => {
            let {results, message, status, errors} = action.payload;
            // console.log('d:::7', results)
            if (status) {
                state.templates = results;
                state.isLoading = 'success';
            } else {
                state.isLoading = 'error';
                state.apiWarning = errors.errorMessage;
            }
        }).addCase(templatesLoader.rejected, state => {
            state.isLoading = 'failed';
        })
    }
});
export const {changeDefaultTemplate, manageNotifications,cleanDefaultTemplate} = TemplatesSlicer.actions;
export default TemplatesSlicer.reducer;