import {configureStore} from '@reduxjs/toolkit'
import UserSlicer from "./slicers/UserSlicer";
import AppSlicer from "./slicers/AppSlicer";
import InventorySlice from "./slicers/InventorySlicer";
import DashboardSlicer from "./slicers/DashboardSlicer";
import SubscriptionSlicer from "./slicers/SubscriptionSlicer";
import TemplatesSlicer from "./slicers/TemplatesSlicer";

export default configureStore({
    reducer: {
        dashboard: DashboardSlicer,
        user: UserSlicer,
        app: AppSlicer,
        inventory: InventorySlice,
        subscription: SubscriptionSlicer,
        templating: TemplatesSlicer
    }
})