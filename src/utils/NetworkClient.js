import store from "../redux/store";
import axios from "axios";
import {API_URL, CDN_URL} from "./Resources";
import {loginUserThunk, logoutUser} from "../redux/slicers/UserSlicer";
// import store from "../redux/store";
const client = axios;

export const postMethod = (token, path, body) => {
    return client.post(API_URL + path, JSON.stringify(body), {
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        }
    }).then(res => res)
        .catch(err => err);
};

export const postCDN = (path, body) => {
    return client.post(CDN_URL + path, JSON.stringify(body), {
        headers: {
            'Content-type': 'application/json',
        }
    }).then(res => res)
        .catch(err => err);
};

export const getDate = (token, path) => {
    return client.get(API_URL + path, {
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        }
    }).then(res => res)
        .catch(err => err);
};

export const updateData = (token, path, body) => {
    return client.patch(API_URL + path, JSON.stringify(body), {
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        }
    }).then(res => res)
        .catch(err => err);
};
export const replaceData = (token, path, body) => {
    return client.put(API_URL + path, JSON.stringify(body), {
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        }
    }).then(res => res)
        .catch(err => err);
};

export const deleteData = (token, path) => {
    return client.delete(API_URL + path, {
        headers: {
            'Content-type': 'application/json',
            'Authorization': token
        }
    }).then(res => res)
        .catch(err => err);
}


const refresh = axios.create();
// const {dispatch} = store;
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    // when there is an error that is outside of unauthorized scope we need to log that error.
    if (error.response.status >= 415) {
        //  log the error, and send it to the API to Email it to Support Team.
        //  we're using the interceptors to catch the error because its global and used by all of axios implementations.
        //  console.log(error.response); error response is containing the error object the status code and many useful information about
        //  what happened in here that cause the error.
        // dispatch(submitErrorLog({source: 'intake form', log_message: error.response}));
    }

    const originalRequest = error.config;
    if (error.response.status === 401) {
        // do a refresh and retry the request.
        localStorage.removeItem('user');
        // dispatch(logoutUser());
        // alert('401');

        // console.log('d::: ', window.gapi.auth2.getAuthInstance().j8.currentUser.Ab.reloadAuthResponse() )
        // let refreshToken  =  window.gapi.auth2.getAuthInstance().currentUser.get().reloadAuthResponse()
        // console.log('d:::', refreshToken);
        // return  refreshToken;
        // refresh.get('https://oauth2.googleapis.com/tokeninfo?id_token=' + localStorage.getItem('googleToken'))
        //     .then(res => {
        //         console.log('d:::1', res.data);
        //     }).catch(err => {
        //     console.log('d:::2', err);
    // }
// )
    // return refresh.post(API_URL + 'auth/login', JSON.stringify({
    //     UserEmail: "Jagger@gta-canada.com",
    //     Password: "gta4all"
    // }), {
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'Access-Control-Allow-Origin': '*',
    //         'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    //     }
    // }).then((responseData) => {
    //     console.log('res::: ', responseData.data);
    //     // inject the new token.
    //     originalRequest.headers['Authorization'] = 'Bearer ' + responseData.data.token;
    //     // re chain the request
    //     return axios(originalRequest);
    // }).catch((error) => {
    //     console.log("error::: ", error);
    // });
}
return Promise.reject(error);
})
;