import DashboardIcon from '@mui/icons-material/Dashboard';
import SettingsIcon from '@mui/icons-material/Settings';
import ShoppingBagIcon from '@mui/icons-material/ShoppingBag';
import HelpCenterIcon from '@mui/icons-material/HelpCenter';
import {AccountBalanceWallet} from "@mui/icons-material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import StoreIcon from '@mui/icons-material/Store';
import ReceiptIcon from '@mui/icons-material/Receipt';
export const NavMenu = [
    {
        title: 'Dashboard',
        path: '/app/dashboard',
        icon: <DashboardIcon/>,
        disabled: false
    }, {
        title: 'Settings',
        path: '/app/settings',
        icon: <SettingsIcon/>,
        disabled: false
    },{
        title: 'Inventory',
        path: '/app/inventory',
        icon: <StoreIcon/>,
        disabled: false
    }, {
        title: 'Wallet',
        path: '/app/wallet',
        icon: <AccountBalanceWallet/>,
        disabled: true
    }, {
        title: 'Market Place',
        path: '/app/market-place',
        icon: <ShoppingBagIcon/>,
        disabled: false
    },{
        title: 'Orders',
        path: '/app/orders',
        icon: <ReceiptIcon/>,
        disabled: false
    }, {
        title: 'profile',
        path: '/app/profile',
        icon: <AccountCircleIcon/>,
        disabled: false
    }, {
        title: 'Support',
        path: '/app/support',
        icon: <HelpCenterIcon/>,
        disabled: false
    },
];