import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import logo from '../../assets/images/logo.png';
import {useDispatch, useSelector} from "react-redux";
import {loginUserThunk} from "../../redux/slicers/UserSlicer";
import GoogleLogin from "react-google-login";
import useMediaQuery from "@mui/material/useMediaQuery";
import SystemBackdrop from "../../components/backdrop/SystemBackdrop";
import {Navigate} from "react-router-dom";
import SystemAlerts from "../../components/alert/SystemAlerts";
import {AppBar, Button, Grid} from "@mui/material";
import Paper from "@mui/material/Paper";
import GoogleIcon from "@mui/icons-material/Google";


// import GoogleLogin from "react-google-login";
function Copyright(props) {
    return (
        <Typography variant="body2" color={'inherit'} align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" target='_blank' href="https://yafatek.dev">
                YafaTek Solutions
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}


export default function Login() {
    const dispatch = useDispatch();
    const {isLoading, isAuth, apiError, apiSuccess, openNotification} = useSelector((state) => state.user);
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    // const [openNotification, setOpenNotification] = useState(false);
    const theme = React.useMemo(
        () =>
            createTheme({
                // direction: langDirection,
                direction: 'ltr',
                // components: {
                //     MuiButtonBase: {
                //         defaultProps: {
                //             // The props to apply
                //             disableRipple: true, // No more ripple, on the whole application 💣!
                //         },
                //     },
                // },
                palette: {
                    mode: prefersDarkMode ? 'dark' : 'light',
                    // mode: prefersDarkMode ? 'dark' : 'light',
                    primary: {
                        // light: will be calculated from palette.primary.main,
                        main: '#2bb3c0',
                        // main: 'rgb(21, 50, 44)',
                        // dark: will be calculated from palette.primary.main,
                        contrastText: 'rgb(21, 50, 44)',
                        // contrastText: 'rgb(21, 50, 44)',
                    },
                    custom: {
                        main: '#424242'
                    }
                },
                root: {},
                typography: {
                    fontFamily: [
                        "Open Sans Condensed",
                        'sans-serif',
                    ].join(','),
                },
            }),
        [prefersDarkMode],
    );
    const handleGoogleLogin = values => {
        // console.log('d::: ', JSON.stringify(values));
        localStorage.setItem('googleToken', values.tokenId);
        // login the system to obtain a valid JWT.
        dispatch(loginUserThunk({googleToken: values.tokenId}));
    };
    const handleNotificationClose = () => {

    }
    let content;
    if (isLoading) {
        content = <SystemBackdrop open={true}/>
    } else {
        content =
            <ThemeProvider theme={theme}>
                <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    sx={{minHeight: '100vh'}}
                >

                    <Grid item xs={3}>
                        <Paper elevation={24}
                               sx={{
                                   backgroundColor: 'rgb(54, 79, 107)',
                                   width: 400,
                               }}
                        >
                            <CssBaseline/>
                            {apiSuccess ? <SystemAlerts
                                type={'success'}
                                open={openNotification}
                                message={apiSuccess}
                                onClose={handleNotificationClose}
                            /> : ''}
                            {apiError ? <SystemAlerts
                                type={'error'}
                                open={openNotification}
                                message={apiError}
                                onClose={handleNotificationClose}
                            /> : ''}
                            <Box
                                sx={{
                                    // marginTop: 8,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',

                                }}
                                // color={'primary.custom'}
                            >
                                <Box sx={{
                                    p: 1,
                                    m: 1
                                }}>
                                    <img
                                        height={'250'}
                                        width={'300'}
                                        src={logo}
                                        alt='logo'/>
                                </Box>

                                <Typography component="h1" variant="h5">
                                    Hi, Welcome Back
                                </Typography>
                            </Box>
                            <Box
                                sx={{
                                    marginTop: 4,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}
                            >
                                <GoogleLogin
                                    clientId="241785082240-jpvvgj0o5i4cas82kmdsp16595p4d5bn.apps.googleusercontent.com"
                                    render={renderProps => (
                                        <Button
                                            color={'success'}
                                            startIcon={<GoogleIcon/>} onClick={renderProps.onClick}
                                            sx={{
                                                border: 'solid 2px #2bb3c0',
                                                color: 'white',
                                                borderRadius: '2px solid #7635dc !important',
                                                '&:hover': {
                                                    color: 'white',
                                                }
                                            }}>
                                            Login with Google
                                        </Button>

                                    )}
                                    onSuccess={handleGoogleLogin}
                                    onFailure={handleGoogleLogin}
                                    cookiePolicy={'single_host_origin'}
                                    accessType="offline"
                                    // responseType="code"
                                    // isSignedIn={true}
                                    // approvalPrompt="force"
                                    // // responseType="code"
                                    // prompt="consent"
                                />

                            </Box>
                            <Box
                                sx={{
                                    marginTop: 5,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}
                            >
                                <Typography variant="body2" align="center" sx={{m: 3}}>
                                    GIDA Commerce System is a Trade Mark of YafaTek</Typography>
                            </Box>
                            <AppBar position="fixed" color={'inherit'} sx={{top: 'auto', bottom: 0}}>
                                <Copyright/>
                            </AppBar>
                            {/*<Copyright sx={{mt: 8, mb: 4, color: 'primary.main'}}/>*/}
                        </Paper>
                    </Grid>

                </Grid>
            </ThemeProvider>
    }

    if (isAuth) {
        return <Navigate to='/app/dashboard'/>
    }
    return (
        content
    );
}
