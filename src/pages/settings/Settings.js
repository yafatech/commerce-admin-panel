import React, {useEffect, useState} from "react";
import ThemeContainer from "../../components/layout/theme/ThemeContainer";
import {Box, Grid, Paper, Typography} from "@mui/material";
import SysTabs from "../../components/widgets/SysTabs";
import TabPanel from "../../components/widgets/TabPanel";
import ManagedWebSitesForm from "../../components/settings/ManagedWebSitesForm";
import {useDispatch, useSelector} from "react-redux";
import {deleteWebsite, loadQrData, manageInvNotifications, removeWebsite} from "../../redux/slicers/InventorySlicer";
import {
    changeDefaultTemplate,
    manageNotifications,
    templatesLoader
} from "../../redux/slicers/TemplatesSlicer";
import {manageAppNotifications} from '../../redux/slicers/AppSlicer'

import {Navigate} from "react-router-dom";
import SystemAlerts from "../../components/alert/SystemAlerts";
import ManagedBusinessCard from "../../components/settings/ManagedBusinessCard";
import ConfirmDialog from "../../components/alert/ConfirmDialog";
import EditQrModal from "../../components/modals/EditQrModal";
import MapSelector from "../../components/widgets/maps/MapSelector";
import BusinessInfoForm from "../../components/settings/BusinessInfoForm";

const tabsArray = [
    {
        index: 0,
        label: 'Manage Websites'
    }, {
        index: 1,
        label: 'Business Information'
    }
    // , {
    //     index: 2,
    //     label: 'Three'
    // }, {
    //     index: 3,
    //     label: 'One'
    // },
]
export default function Settings() {
    const [value, setValue] = React.useState(0);
    const [qrId, setQrId] = useState('');
    const [itemToBeEdited, setItemToBeEdited] = useState(undefined);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [isEditQrOpened, setIsEditQrOpened] = useState(false);
    const {token, userId} = useSelector(state => state.user.user);
    const {isAuth} = useSelector(state => state.user);
    const dispatch = useDispatch();
    const {
        isLoading,
        apiSuccess,
        apiError,
        apiWarning,
        openNotification,
        templates
    } = useSelector(state => state.templating);


    const {managedBusinesses} = useSelector(state => state.inventory);
    const invApiSuccess = useSelector(state => state.inventory.apiSuccess);
    const invApiError = useSelector(state => state.inventory.apiError);
    const invApiWarning = useSelector(state => state.inventory.apiWarning);
    const invOpenNotification = useSelector(state => state.inventory.openNotification);
    const isInvLoading = useSelector(state => state.inventory.isLoading);
    const openMapNotification = useSelector(state => state.app.openNotification);

    const handleNotificationClose = () => {
        dispatch(manageNotifications({status: false}))
    }

    const handleInvNotificationClose = () => {
        dispatch(manageInvNotifications({status: false}))
    }

    const handleTabChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleIndexChange = (index) => {
        setValue(index);
    }

    const handleEditQrModalOpen = (item) => {
        setIsEditQrOpened(!isEditQrOpened);
        // push to edit template.
        let defaultTemplate = templates.find(item => item.templateContainerName === item.templateContainerName)
        dispatch(changeDefaultTemplate(defaultTemplate));

        // push the item to be edited.
        setItemToBeEdited(item);
    }

    const deleteQR = (confirmed) => {
        if (confirmed) {
            setIsDeleteOpen(false);
            // remove from the backend.
            dispatch(deleteWebsite({token, id: qrId}))
            // remove from local list if the api request id done.
            if (isInvLoading === 'success') {
                dispatch(removeWebsite(qrId));
            }
        } else {
            setIsDeleteOpen(false);
        }

    }
    const handleDeleteWebsite = (qrId) => {
        setIsDeleteOpen(true);
        setQrId(qrId);
    }

    let managedBusinessesContnet;
    if (!managedBusinesses.length) {
        managedBusinessesContnet =
            <Typography variant={'h5'} color={'primary'}>oops, there is no data to show</Typography>
    } else {
        managedBusinessesContnet = <Grid container spacing={1}
                                         sx={{color: 'white', overflow: 'auto', height: 600, m: 1}}>
            {managedBusinesses.map((item, index) => {
                return <Grid item md={5} xs={5} sm={2}>
                    <ManagedBusinessCard item={item} handleDeleteWebsite={handleDeleteWebsite}
                                         handleEditQrModalOpen={handleEditQrModalOpen}/>
                </Grid>
            })}

        </Grid>
    }


    useEffect(() => {
        if (token && userId) {
            dispatch(loadQrData({token, userId}));
        }
        if (isLoading === 'idle') {
            dispatch(templatesLoader());
        }
    }, []);

    if (!isAuth) {
        return <Navigate to='/'/>
    }
    return (
        <ThemeContainer>

            <Paper elevation={22} sx={{
                alignContent: 'center',
                alignItems: 'center',
                background: 'inherit'
            }}>

                <div>
                    {openMapNotification ? <SystemAlerts
                        type={'error'}
                        open={openMapNotification}
                        message={'Location is not enabled...'}
                        onClose={() => dispatch(manageAppNotifications(false))}
                    /> : ''}
                    {invApiSuccess ? <SystemAlerts
                        type={'success'}
                        open={invOpenNotification}
                        message={invApiSuccess}
                        onClose={handleInvNotificationClose}
                    /> : ''}
                    {invApiError ? <SystemAlerts
                        type={'error'}
                        open={invOpenNotification}
                        message={invApiError}
                        onClose={handleInvNotificationClose}
                    /> : ''}
                    {invApiWarning ? <SystemAlerts
                        type={'warning'}
                        open={invOpenNotification}
                        message={invApiWarning}
                        onClose={handleInvNotificationClose}
                    /> : ''}
                    {apiSuccess ? <SystemAlerts
                        type={'success'}
                        open={openNotification}
                        message={apiSuccess}
                        onClose={handleNotificationClose}
                    /> : ''}
                    {apiError ? <SystemAlerts
                        type={'error'}
                        open={openNotification}
                        message={apiError}
                        onClose={handleNotificationClose}
                    /> : ''}
                    {apiWarning ? <SystemAlerts
                        type={'warning'}
                        open={openNotification}
                        message={apiWarning}
                        onClose={handleNotificationClose}
                    /> : ''}

                </div>

                <SysTabs tabArray={tabsArray} value={value} handleIndexChange={handleIndexChange}
                         handleChange={handleTabChange}>
                    <TabPanel value={value} index={0}>
                        <Grid container sx={{color: 'white'}}>
                            {/*<Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}} sx={{color: 'white'}}>*/}
                            <Grid item sm={4} md={4} lg={3}>
                                <Paper elevation={24} color={'inherit'} sx={{
                                    m: 2,
                                    background: 'inherit'
                                }}>
                                    <ManagedWebSitesForm loading={isInvLoading === 'loading'}/>
                                </Paper>
                            </Grid>
                            <Grid item sm={3} md={4} lg={9}>
                                <Paper elevation={24} color={'inherit'} sx={{
                                    m: 2,
                                    background: 'inherit'
                                }}>
                                    <Box component={'div'} sx={{
                                        mt: 1,
                                        overflow: 'auto',
                                        width: '100%'
                                    }}>
                                        {managedBusinessesContnet}
                                    </Box>

                                </Paper>
                            </Grid>
                        </Grid>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}} sx={{color: 'white'}}>
                            <Grid item xs={5}>
                                <MapSelector/>
                            </Grid>
                            <Grid item xs>
                                <Paper elevation={24} color={'inherit'} sx={{
                                    m: 2,
                                    background: 'inherit',
                                    overflow: 'auto',
                                    height: 600
                                }}>
                                    <BusinessInfoForm/>
                                </Paper>
                            </Grid>
                        </Grid>
                    </TabPanel>
                </SysTabs>
                <ConfirmDialog title="Deleting a Website QR"
                               message="are you sure you want to delete Website? all categories and products will be deleted, this can't be reversed"
                               open={isDeleteOpen}
                               handleClick={deleteQR}
                               color={'error'}
                />
                <EditQrModal
                    open={isEditQrOpened}
                    item={itemToBeEdited}
                    handleClose={handleEditQrModalOpen}
                />
            </Paper>

        </ThemeContainer>
    )
}