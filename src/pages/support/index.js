import React from "react";
import {Button, Grid, Paper, Typography} from "@mui/material";
import ThemeContainer from "../../components/layout/theme/ThemeContainer";
import {useSelector} from "react-redux";
import {Navigate} from "react-router-dom";

const gridSpacing = 3;
export default function Support(props) {
    const {isAuth} = useSelector(state => state.user);
    if (!isAuth) {
        return <Navigate to='/'/>
    }
    return (
        <ThemeContainer>
            <Grid container spacing={gridSpacing} textAlign={'center'}>
                <Grid item xs>
                    <Paper
                        elevation={24}
                        sx={{
                            background: 'inherit',
                            height: 400,
                            p: 1,
                            m: 1
                        }}>

                        <Typography variant={'body1'} color={'primary'}>
                            Coming soon
                        </Typography>
                        <Button variant={'outlined'} onClick={(e) => e.preventDefault()}>Dashboard</Button>
                    </Paper>
                </Grid>
            </Grid>
        </ThemeContainer>
    )
}