import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from "@mui/material/CardMedia";
import headerImage from '../../assets/images/profile/headerback.png';
import {Avatar, Stack} from "@mui/material";
import {useState} from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

const bull = (
    <Box
        component="span"
        sx={{display: 'inline-block', mx: '2px', transform: 'scale(0.8)'}}
    >
        •
    </Box>
);

const tabsArray = [
    {
        index: 0,
        label: 'One'
    }, {
        index: 1,
        label: 'Two'
    }, {
        index: 2,
        label: 'Three'
    }, {
        index: 3,
        label: 'One'
    },
]
export default function ProfileHeader() {
    const [value, setValue] = React.useState(0);
    const [index, setIndex] = useState(0);
    const handleTabChange = () => {

    }
    const handleIndexChange = () => {

    }
    return (
        <Card sx={{minWidth: 275, boxShadow: 'inherit', borderRadius: 2}} raised={true}>
            <CardMedia
                component="img"
                height="194"
                image={headerImage}
                alt="profile header image."
            />
            <CardContent>
                <Avatar sx={{zIndex: 1, bottom: 'auto', top: '-40px', width: 64, height: 64}}>
                    A
                </Avatar>
                <Stack
                    direction="row"
                    justifyContent="flex-end"
                    alignItems="flex-end"
                    spacing={2}
                >
                    <Tabs value={value} onChange={handleTabChange} aria-label="disabled tabs example">
                        <Tab label="Active" />
                        <Tab label="Disabled" disabled />
                        <Tab label="Active" />
                    </Tabs>
                </Stack>
            </CardContent>
        </Card>
    );
}
