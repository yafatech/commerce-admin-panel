import React, {useEffect} from "react";
import ThemeContainer from "../../components/layout/theme/ThemeContainer";
import {useDispatch, useSelector} from "react-redux";
import {Navigate} from "react-router-dom";
import SystemBackdrop from "../../components/backdrop/SystemBackdrop";
import {loadQrData} from "../../redux/slicers/InventorySlicer";
import ProfileHeader from "./ProfileHeader";

export default function Profile() {
    let {isAuth, userId, token, isLoading} = useSelector(state => state.user);
    // let {isLoading} = useSelector(state => state.inventory);
    const dispatch = useDispatch();
    let content;

    if (isLoading) {
        content = <SystemBackdrop open={true}/>
    } else {
        content =<>
            <ProfileHeader/>
        </>

            {/*<Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}}>*/}
            {/*    <Grid item xs={3}>*/}
            {/*        pppp*/}
            {/*    </Grid>*/}
            {/*</Grid>*/}
        // </div>
    }

    useEffect(() => {
        if (isLoading === 'idle') {
            // load the inventory data.
            dispatch(loadQrData({token, id: userId}))
        }

    }, []);
    if (!isAuth) {
        return <Navigate to='/ '/>
    }

    return (
        <ThemeContainer>
            {content}
        </ThemeContainer>
    )
}