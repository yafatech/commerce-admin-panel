import React, {useEffect, useState} from "react";
import {Box, Button, Grid, Paper, Typography} from "@mui/material";
import ThemeContainer from "../../components/layout/theme/ThemeContainer";
import SysTabs from "../../components/widgets/SysTabs";
import TabPanel from "../../components/widgets/TabPanel";
import {useDispatch, useSelector} from "react-redux";
import AddCategoryWidget from "../../components/inventory/AddCategoryWidget";
import {Navigate} from "react-router-dom";
import {handleOpenNotificationChange, loadOwnerCategories, loadQrData} from "../../redux/slicers/InventorySlicer";
import SystemAlerts from "../../components/alert/SystemAlerts";
import ViewCategoryWidget from "../../components/inventory/ViewCategoryWidget";

const gridSpacing = 3;
const tabsArray = [
    {
        index: 0,
        label: 'Categories'
    }, {
        index: 1,
        label: 'Products'
    }
];
export default function Inventory() {
    const [value, setValue] = useState(0);
    const [openAddCategory, setOpenAddCategory] = useState(false);
    const {
        categories,
        isLoading,
        openNotification,
        apiError,
        apiWarning,
        apiSuccess
    } = useSelector(state => state.inventory);
    const {isAuth} = useSelector(state => state.user);
    const {token, userId} = useSelector(state => state.user.user);

    const dispatch = useDispatch();
    const handleOpenAddCategory = () => {
        setOpenAddCategory(!openAddCategory);
    }

    const handleTabChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleIndexChange = (index) => {
        setValue(index);
    }

    let renderCategories;
    if (categories.length > 0) {
        renderCategories = <Grid container>
            {categories.map((item) => {
                return <Grid item sm={6} md={3} lg={2} m={1}>
                    <ViewCategoryWidget item={item}/>
                </Grid>
            })}
        < /Grid>
    } else {
        renderCategories =
            <Typography variant={'h6'} sx={{m: '0 auto',}}> please add your first category.</Typography>
    }
    useEffect(() => {
        if (isLoading === 'idle') {
            dispatch(loadQrData({token, userId}));
            // load categories.
            dispatch(loadOwnerCategories({token, userId}))
        }
    }, []);

    if (!isAuth) {
        return <Navigate to='/'/>
    }

    return (
        <ThemeContainer>
            <div>
                {openNotification ? <SystemAlerts
                    type={'error'}
                    open={openNotification}
                    message={apiError}
                    onClose={() => dispatch(handleOpenNotificationChange(false))}
                /> : ''}
            </div>
            <SysTabs tabArray={tabsArray} value={value} handleIndexChange={handleIndexChange}
                     handleChange={handleTabChange}>
                <TabPanel value={value} index={0}>
                    <Grid container>
                        <Grid item sm={12} md={12} lg={12}>
                            <Paper elevation={24} color={'inherit'}
                                   sx={{
                                       // m: 2,
                                       p: 2,
                                       height: 600,
                                       // height: '100%',
                                       background: 'inherit',
                                       overflow: 'auto'
                                   }}>
                                <Button sx={{
                                    m: 1
                                }}
                                        variant={'outlined'}
                                        color={'primary'}
                                        onClick={() => handleOpenAddCategory()}>
                                    Add New
                                </Button>
                                <Box component={'div'} sx={{
                                    color: 'white',
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "center"
                                }}>
                                    {renderCategories}
                                </Box>
                            </Paper>
                        </Grid>
                    </Grid>
                </TabPanel>
            </SysTabs>
            <AddCategoryWidget open={openAddCategory} handleClose={handleOpenAddCategory}
                               title={'add new Category'}/>
        </ThemeContainer>
    )
}