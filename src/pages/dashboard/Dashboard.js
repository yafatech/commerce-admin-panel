import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Navigate} from "react-router-dom";
import ThemeContainer from "../../components/layout/theme/ThemeContainer";
import {Grid} from "@mui/material";
import {loadDashboardStatistics} from "../../redux/slicers/DashboardSlicer";
import {checkUserSubscription} from "../../redux/slicers/SubscriptionSlicer";
import EarningCard from "../../components/cards/EarningCard";
import TotalOrderLineChartCard from "../../components/cards/dashboard/TotalOrderLineChartCard";
import TotalIncomeDarkCard from "../../components/cards/dashboard/TotalIncomeDarkCard";
import TotalIncomeLightCard from "../../components/cards/dashboard/TotalIncomeLightCard";
import PopularCard from "../../components/cards/dashboard/PopularCard";
import TotalGrowthBarChart from "../../components/cards/dashboard/TotalGrowthBarChart";

export default function Dashboard(props) {
    const {isAuth} = useSelector(state => state.user);
    const {token, userId} = useSelector(state => state.user.user);
    const {isLoading, statistics} = useSelector(state => state.dashboard);
    const isSubLoading = useSelector(state => state.subscription.isLoading);
    const dispatch = useDispatch();
    const gridSpacing = 3;

    useEffect(() => {
        if (isLoading === 'idle') {
            // load the data.
            dispatch(loadDashboardStatistics({token, userId}));
        }
        if (isSubLoading === 'idle') {
            // load user subscription.
            dispatch(checkUserSubscription({token, userId}))
        }
    }, []);

    // let content;
    // if (isLoading === 'loading') {
    //     content = <SystemBackdrop open={true}/>
    // } else {
    //     content = ''
    //     /*
    //     <>
    //     <Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}}>
    //         <Grid item xs={3}>
    //             <StatisticsWidgets title={'Top Product'} desc={statistics.productName} cardImg={mission}/>
    //         </Grid>
    //         <Grid item xs={3}>
    //             <StatisticsWidgets title={'Happy Customers'} desc={statistics.happyCustomers}
    //                                cardImg={happyCustomers}/>
    //         </Grid>
    //         <Grid item xs={3}>
    //             <StatisticsWidgets title={'Weekly Sales'} desc={'$' + statistics.weeklySales} cardImg={profits}/>
    //         </Grid>
    //         <Grid item xs={3}>
    //             <StatisticsWidgets title={'Monthly Sales'} desc={'$' + statistics.monthlySales} cardImg={rich}/>
    //         </Grid>
    //     </Grid>
    //     <br/>
    //     <Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}}>
    //         <Grid item xs>
    //             <RecentOrders/>
    //         </Grid>
    //         <Grid item xs={3}>
    //             <TopSalesProduct/>
    //         </Grid>
    //     </Grid>
    // </>
    //
    //      */
    // }

    if (!isAuth) {
        return <Navigate to='/'/>
    }
    return (
        <ThemeContainer>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <EarningCard isLoading={isLoading}/>
                        </Grid>
                        <Grid item lg={4} md={6} sm={6} xs={12}>
                            <TotalOrderLineChartCard isLoading={isLoading}/>
                        </Grid>
                        <Grid item lg={4} md={12} sm={12} xs={12}>
                            <Grid container spacing={gridSpacing}>
                                <Grid item sm={6} xs={12} md={6} lg={12}>
                                    <TotalIncomeDarkCard isLoading={isLoading}/>
                                </Grid>
                                <Grid item sm={6} xs={12} md={6} lg={12}>
                                    <TotalIncomeLightCard isLoading={isLoading}/>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} md={8}>
                            <TotalGrowthBarChart isLoading={isLoading}/>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <PopularCard isLoading={isLoading}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </ThemeContainer>
    );
};