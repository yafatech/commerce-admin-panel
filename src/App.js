import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import Login from "./pages/user/Login";
import Dashboard from "./pages/dashboard/Dashboard";
import Profile from "./pages/profile/Profile";
import Settings from "./pages/settings/Settings";
import React from "react";
import MarketPlace from "./pages/marketplace/MarketPlace";
import Orders from "./pages/orders";
import Support from "./pages/support";
import Inventory from "./pages/inventory";

export default function App() {

    return (
        <div className={'App'}>
            <Router>
                <Routes>
                    <Route exact path='/' element={<Login/>}/>
                    <Route exact path='/app/dashboard' element={<Dashboard/>}/>
                    <Route exact path='/app/profile' element={<Profile/>}/>
                    <Route exact path='/app/inventory' element={<Inventory/>}/>
                    <Route exact path='/app/settings' element={<Settings/>}/>
                    <Route exact path='/app/market-place' element={<MarketPlace/>}/>
                    <Route exact path='/app/orders' element={<Orders/>}/>
                    <Route exact path='/app/support' element={<Support/>}/>
                </Routes>
            </Router>
        </div>
    );
};
